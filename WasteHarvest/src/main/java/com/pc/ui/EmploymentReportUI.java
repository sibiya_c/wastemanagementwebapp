package com.pc.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.beans.EmploymentBean;
import com.pc.beans.LabelTotalBean;
import com.pc.entities.enums.EmploymentStatusEnum;
import com.pc.entities.lookup.BBC;
import com.pc.framework.AbstractUI;
import com.pc.service.ReportService;

@Component("employmentReportUI")
@ViewScoped
public class EmploymentReportUI extends AbstractUI{

  @Autowired
  ReportService reportService;
  List<EmploymentBean>  employmentBeanList;
  private Map<String, List<LabelTotalBean>> mappedList;
  List<LabelTotalBean> maleList;
  List<LabelTotalBean> femaleList;
  
  List<EmploymentBean>  employmentByRoleBeanList;
  private Map<String, List<LabelTotalBean>> empByRoleMappedList;
  List<LabelTotalBean> empByRoleMaleList;
  List<LabelTotalBean> empByRoleFemaleList;
  private BBC bbc;
  
  private EmploymentStatusEnum employmentStatus;
  
  
  @PostConstruct
  public void init() {
	  
		  try
		  {
			prepareCurrentUser();
			if(currentUser.getBbc() !=null){
				bbc=currentUser.getBbc();
			}
			reloadGrapgData();
			reloadEmpByRoleGrapgData();
		  }
		  catch (Exception e) {
				e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}
	
	
	public void clearEmpStatus()
	{
		employmentStatus=null;
		reloadGrapgData();
	}
    public void reloadGrapgData() {
        try {
			mappedList = new HashMap<>();
			maleList = new ArrayList<>();
			femaleList = new ArrayList<>();
			employmentBeanList=reportService.findEmploymentStatusReport(employmentStatus);
			for(EmploymentBean empBean:employmentBeanList)
			{
				LabelTotalBean male=new LabelTotalBean(empBean.getDescription(), new Long(empBean.getTotalMale()));
				LabelTotalBean female=new LabelTotalBean(empBean.getDescription(), new Long(empBean.getTotalFemale()));
				maleList.add(male);
				femaleList.add(female);
			}
			mappedList.put("Males", maleList);
			mappedList.put("Females", femaleList);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
    }
    
    public void clearEmpByRoleEmpStatus()
	{
		employmentStatus=null;
		if(currentUser.getBbc() !=null){
			bbc=currentUser.getBbc();
		}
		reloadEmpByRoleGrapgData();
	}
    public void reloadEmpByRoleGrapgData() {
        try {
			empByRoleMappedList = new HashMap<>();
			empByRoleMaleList = new ArrayList<>();
			empByRoleFemaleList = new ArrayList<>();
			employmentByRoleBeanList=reportService.findEmploymentStatusPerRoleReport(employmentStatus,bbc);
			for(EmploymentBean empBean:employmentByRoleBeanList)
			{
				LabelTotalBean male=new LabelTotalBean(empBean.getDescription(), new Long(empBean.getTotalMale()));
				LabelTotalBean female=new LabelTotalBean(empBean.getDescription(), new Long(empBean.getTotalFemale()));
				empByRoleMaleList.add(male);
				empByRoleFemaleList.add(female);
			}
			empByRoleMappedList.put("Males", empByRoleMaleList);
			empByRoleMappedList.put("Females", empByRoleFemaleList);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
    }
    
	public Map<String, List<LabelTotalBean>> getMappedList() {
		return mappedList;
	}
	public void setMappedList(Map<String, List<LabelTotalBean>> mappedList) {
		this.mappedList = mappedList;
	}
	public List<LabelTotalBean> getMaleList() {
		return maleList;
	}
	public void setMaleList(List<LabelTotalBean> maleList) {
		this.maleList = maleList;
	}
	public List<LabelTotalBean> getFemaleList() {
		return femaleList;
	}
	public void setFemaleList(List<LabelTotalBean> femaleList) {
		this.femaleList = femaleList;
	}
	public EmploymentStatusEnum getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(EmploymentStatusEnum employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
   
	public List<EmploymentBean> getEmploymentBeanList() {
		return employmentBeanList;
	}
	public void setEmploymentBeanList(List<EmploymentBean> employmentBeanList) {
		this.employmentBeanList = employmentBeanList;
	}


	public List<EmploymentBean> getEmploymentByRoleBeanList() {
		return employmentByRoleBeanList;
	}


	public void setEmploymentByRoleBeanList(List<EmploymentBean> employmentByRoleBeanList) {
		this.employmentByRoleBeanList = employmentByRoleBeanList;
	}


	public Map<String, List<LabelTotalBean>> getEmpByRoleMappedList() {
		return empByRoleMappedList;
	}


	public void setEmpByRoleMappedList(Map<String, List<LabelTotalBean>> empByRoleMappedList) {
		this.empByRoleMappedList = empByRoleMappedList;
	}



	public List<LabelTotalBean> getEmpByRoleFemaleList() {
		return empByRoleFemaleList;
	}


	public void setEmpByRoleFemaleList(List<LabelTotalBean> empByRoleFemaleList) {
		this.empByRoleFemaleList = empByRoleFemaleList;
	}


	public List<LabelTotalBean> getEmpByRoleMaleList() {
		return empByRoleMaleList;
	}


	public void setEmpByRoleMaleList(List<LabelTotalBean> empByRoleMaleList) {
		this.empByRoleMaleList = empByRoleMaleList;
	}


	public BBC getBbc() {
		return bbc;
	}


	public void setBbc(BBC bbc) {
		this.bbc = bbc;
	}
}
