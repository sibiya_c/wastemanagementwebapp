package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.entities.*;
import com.pc.entities.lookup.*;
import com.pc.framework.AbstractUI;
import com.pc.service.*;
import com.pc.service.lookup.*;
import com.pc.entities.enums.ApprovalStatusEnum;
import com.pc.entities.enums.EmailTemplateTypeEnum;
import com.pc.entities.enums.EmploymentStatusEnum;
import com.pc.entities.enums.PermissionEnum;
import com.pc.entities.enums.ReportTypeEnum;
import com.pc.entities.enums.ReportedInstitutionsStatus;
import com.pc.entities.enums.SearchTypeEnum;
import com.pc.entities.enums.YesNoEnum;


@Component("commonItemUI")
@ViewScoped
public class CommonItemUI  extends AbstractUI
{
	@Autowired
	GenderService genderService; 
	
	
	
	@Autowired
	TitleService titleService;
	
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	@PostConstruct
	public void init() {
		
	}
	
	/**
	 * Gets the select items gender.
	 *
	 * @return the select items gender
	 */
	public List<Gender> getSelectItemsGender() {
		
		List<Gender> l = null;
		try {
			
			l = genderService.findAllGender();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	/**
	 * Gets the select items title.
	 *
	 * @return the select items title
	 */
	public List<Title> getSelectItemsTitle() {
		
		List<Title> l = null;
		try {
			
			l = titleService.findAllTitle();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	public List<SelectItem> getYesNoEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (YesNoEnum val : YesNoEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getEmploymentStatusEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (EmploymentStatusEnum val : EmploymentStatusEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getReportTypeEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ReportTypeEnum val : ReportTypeEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getSearchTypeEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (SearchTypeEnum val : SearchTypeEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
  
    
  public List<Role> getSelectItemsRole() {
		
		List<Role> l = null;
		try {
			
			l = roleService.findAllRole();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
    
   
    
	public List<SelectItem> getReportedInstitutionsStatusDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ReportedInstitutionsStatus val : ReportedInstitutionsStatus.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getPermissionDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (PermissionEnum val : PermissionEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getEmailTemplateTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (EmailTemplateTypeEnum val : EmailTemplateTypeEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
  
	@Autowired
	UserBrowserInfoService userBrowserInfoService; 
	
	public List<UserBrowserInfo> autoCompleteUserBrowserInfo(String searchText) 
	{
		List<UserBrowserInfo> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=userBrowserInfoService.findAllUserBrowserInfo();
			}
			else
			{
				list=userBrowserInfoService.findByDescriptionStartingWith(searchText);
			}*/
			list=userBrowserInfoService.findAllUserBrowserInfo();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<UserBrowserInfo> getSelectItemsUserBrowserInfo() {
		
		List<UserBrowserInfo> l = null;
		try {
			
			l = userBrowserInfoService.findAllUserBrowserInfo();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	WorkflowService workflowService; 
	
	public List<Workflow> autoCompleteWorkflow(String searchText) 
	{
		List<Workflow> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=workflowService.findAllWorkflow();
			}
			else
			{
				list=workflowService.findByDescriptionStartingWith(searchText);
			}*/
			list=workflowService.findAllWorkflow();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Workflow> getSelectItemsWorkflow() {
		
		List<Workflow> l = null;
		try {
			
			l = workflowService.findAllWorkflow();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	@Autowired
	WorkflowRoleService workflowRoleService; 
	
	public List<WorkflowRole> autoCompleteWorkflowRole(String searchText) 
	{
		List<WorkflowRole> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=workflowRoleService.findAllWorkflowRole();
			}
			else
			{
				list=workflowRoleService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<WorkflowRole> getSelectItemsWorkflowRole() {
		
		List<WorkflowRole> l = null;
		try {
			
			l = workflowRoleService.findAllWorkflowRole();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	EmailTemplateService emailTemplateService; 
	
	public List<EmailTemplate> autoCompleteEmailTemplate(String searchText) 
	{
		List<EmailTemplate> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=emailTemplateService.findAllEmailTemplate();
			}
			else
			{
				list=emailTemplateService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<EmailTemplate> getSelectItemsEmailTemplate() {
		
		List<EmailTemplate> l = null;
		try {
			
			l = emailTemplateService.findAllEmailTemplate();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	WorkFlowRoleUserService workFlowRoleUserService; 
	
	public List<WorkFlowRoleUser> autoCompleteWorkFlowRoleUser(String searchText) 
	{
		List<WorkFlowRoleUser> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=workFlowRoleUserService.findAllWorkFlowRoleUser();
			}
			else
			{
				list=workFlowRoleUserService.findAllWorkFlowRoleUser();
				//list=workFlowRoleUserService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<WorkFlowRoleUser> getSelectItemsWorkFlowRoleUser() {
		
		List<WorkFlowRoleUser> l = null;
		try {
			
			l = workFlowRoleUserService.findAllWorkFlowRoleUser();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	
	
	@Autowired
	UserRoleService userRoleService; 
	
	public List<UserRole> autoCompleteUserRole(String searchText) 
	{
		List<UserRole> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=userRoleService.findAllUserRole();
			}
			else
			{
				list=userRoleService.findByDescriptionStartingWith(searchText);
			}*/
			list=userRoleService.findAllUserRole();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<UserRole> getSelectItemsUserRole() {
		
		List<UserRole> l = null;
		try {
			
			l = userRoleService.findAllUserRole();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	DocConfigService docConfigService; 
	
	public List<DocConfig> autoCompleteDocConfig(String searchText) 
	{
		List<DocConfig> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=docConfigService.findAllDocConfig();
			}
			else
			{
				list=docConfigService.findByDescriptionStartingWith(searchText);
			}*/
			list=docConfigService.findAllDocConfig();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<DocConfig> getSelectItemsDocConfig() {
		
		List<DocConfig> l = null;
		try {
			
			l = docConfigService.findAllDocConfig();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	DocByteService docByteService; 
	
	public List<DocByte> autoCompleteDocByte(String searchText) 
	{
		List<DocByte> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=docByteService.findAllDocByte();
			}
			else
			{
				list=docByteService.findByDescriptionStartingWith(searchText);
			}*/
			list=docByteService.findAllDocByte();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<DocByte> getSelectItemsDocByte() {
		
		List<DocByte> l = null;
		try {
			
			l = docByteService.findAllDocByte();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	DocConfigDetailService docConfigDetailService; 
	
	public List<DocConfigDetail> autoCompleteDocConfigDetail(String searchText) 
	{
		List<DocConfigDetail> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=docConfigDetailService.findAllDocConfigDetail();
			}
			else
			{
				list=docConfigDetailService.findByDescriptionStartingWith(searchText);
			}*/
			list=docConfigDetailService.findAllDocConfigDetail();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<DocConfigDetail> getSelectItemsDocConfigDetail() {
		
		List<DocConfigDetail> l = null;
		try {
			
			l = docConfigDetailService.findAllDocConfigDetail();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	DocConfigDetailActionHistService docConfigDetailActionHistService; 
	
	public List<DocConfigDetailActionHist> autoCompleteDocConfigDetailActionHist(String searchText) 
	{
		List<DocConfigDetailActionHist> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=docConfigDetailActionHistService.findAllDocConfigDetailActionHist();
			}
			else
			{
				list=docConfigDetailActionHistService.findByDescriptionStartingWith(searchText);
			}*/
			list=docConfigDetailActionHistService.findAllDocConfigDetailActionHist();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<DocConfigDetailActionHist> getSelectItemsDocConfigDetailActionHist() {
		
		List<DocConfigDetailActionHist> l = null;
		try {
			
			l = docConfigDetailActionHistService.findAllDocConfigDetailActionHist();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	DocumentLevelService documentLevelService; 
	
	public List<DocumentLevel> autoCompleteDocumentLevel(String searchText) 
	{
		List<DocumentLevel> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=documentLevelService.findAllDocumentLevel();
			}
			else
			{
				list=documentLevelService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<DocumentLevel> getSelectItemsDocumentLevel() {
		
		List<DocumentLevel> l = null;
		try {
			
			l = documentLevelService.findAllDocumentLevel();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	MailLogService mailLogService; 
	
	public List<MailLog> autoCompleteMailLog(String searchText) 
	{
		List<MailLog> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=mailLogService.findAllMailLog();
			}
			else
			{
				list=mailLogService.findByDescriptionStartingWith(searchText);
			}*/
			list=mailLogService.findAllMailLog();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<MailLog> getSelectItemsMailLog() {
		
		List<MailLog> l = null;
		try {
			
			l = mailLogService.findAllMailLog();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	TaskService taskService; 
	
	public List<Task> autoCompleteTask(String searchText) 
	{
		List<Task> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=taskService.findAllTask();
			}
			else
			{
				list=taskService.findByDescriptionStartingWith(searchText);
			}*/
			list=taskService.findAllTask();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Task> getSelectItemsTask() {
		
		List<Task> l = null;
		try {
			
			l = taskService.findAllTask();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	UserTaskService userTaskService; 
	
	public List<UserTask> autoCompleteUserTask(String searchText) 
	{
		List<UserTask> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=userTaskService.findAllUserTask();
			}
			else
			{
				list=userTaskService.findByDescriptionStartingWith(searchText);
			}*/
			list=userTaskService.findAllUserTask();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<UserTask> getSelectItemsUserTask() {
		
		List<UserTask> l = null;
		try {
			
			l = userTaskService.findAllUserTask();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	

	@Autowired
    ProvinceService provinceService;
	public List<Province> getSelectItemsProvince() {
			
		List<Province> l = null;
		try {
			
			l = provinceService.findAllProvince();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	@Autowired
	CategoryService categoryService;
    public List<Category> getSelectItemsCategory() {
		
		List<Category> l = null;
		try {
			
			l = categoryService.findAllCategory();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	    
	   
    @Autowired
    CityService cityService;
    public List<City> getSelectItemsCity(Province province) {
		
		List<City> l = null;
		try {
			
			l = cityService.findByProvince(province);
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	    
	  
		@Autowired
		BankingDetailsService bankingDetailsService; 
		
		public List<BankingDetails> autoCompleteBankingDetails(String searchText) 
		{
			List<BankingDetails> list=null;
			try {
				/*if(searchText==null || searchText.isEmpty())
				{
					list=bankingDetailsService.findAllBankingDetails();
				}
				else
				{
					list=bankingDetailsService.findByDescriptionStartingWith(searchText);
				}*/
				list=bankingDetailsService.findAllBankingDetails();
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		 public List<BankingDetails> getSelectItemsBankingDetails() {
			
			List<BankingDetails> l = null;
			try {
				
				l = bankingDetailsService.findAllBankingDetails();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
		


		
		
		@Autowired
		BuyingBackCenterService buyingBackCenterService; 
		
		public List<BuyingBackCenter> autoCompleteBuyingBackCenter(String searchText) 
		{
			List<BuyingBackCenter> list=null;
			try {
				/*if(searchText==null || searchText.isEmpty())
				{
					list=buyingBackCenterService.findAllBuyingBackCenter();
				}
				else
				{
					list=buyingBackCenterService.findByDescriptionStartingWith(searchText);
				}*/
				list=buyingBackCenterService.findAllBuyingBackCenter();
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		 public List<BuyingBackCenter> getSelectItemsBuyingBackCenter() {
			
			List<BuyingBackCenter> l = null;
			try {
				
				l = buyingBackCenterService.findAllBuyingBackCenter();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
		 
		@Autowired
		UserService userService; 
		
		public List<User> autoCompleteUser(String searchText) 
		{
			List<User> list=null;
			try {
				if(searchText==null || searchText.isEmpty())
				{
					list=userService.getAllUserByCustomerFlag(true);
				}
				else
				{
					list=userService.searchUser(searchText,true);
				}
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		@Autowired
		ItemService itemService;
		 public List<Item> getSelectItemsItem() {
				
				List<Item> l = null;
				try {
					
					l = itemService.findAllItem();
				
				} catch (Exception e) {
					displayErrorMssg(e.getMessage());
				}
				return l;
			}
			
		
		
		@Autowired
		CustomerProductService customerProductService; 
		
		public List<CustomerProduct> autoCompleteCustomerProduct(String searchText) 
		{
			List<CustomerProduct> list=null;
			try {
				/*if(searchText==null || searchText.isEmpty())
				{
					list=customerProductService.findAllCustomerProduct();
				}
				else
				{
					list=customerProductService.findByDescriptionStartingWith(searchText);
				}*/
				list=customerProductService.findAllCustomerProduct();
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		 public List<CustomerProduct> getSelectItemsCustomerProduct() {
			
			List<CustomerProduct> l = null;
			try {
				
				l = customerProductService.findAllCustomerProduct();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
		
		
		@Autowired
		MunicipalityService municipalityService; 
		
		public List<Municipality> autoCompleteMunicipality(String searchText) 
		{
			List<Municipality> list=null;
			try {
				if(searchText==null || searchText.isEmpty())
				{
					list=municipalityService.findAllMunicipality();
				}
				else
				{
					list=municipalityService.findByDescriptionStartingWith(searchText);
				}
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		 public List<Municipality> getSelectItemsMunicipality() {
			
			List<Municipality> l = null;
			try {
				
				l = municipalityService.findAllMunicipality();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
		
		
		@Autowired
		AppPropertyService appPropertyService; 
		
		public List<AppProperty> autoCompleteAppProperty(String searchText) 
		{
			List<AppProperty> list=null;
			try {
				if(searchText==null || searchText.isEmpty())
				{
					list=appPropertyService.findAllAppProperty();
				}
				else
				{
					list=appPropertyService.findByDescriptionStartingWith(searchText);
				}
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		 public List<AppProperty> getSelectItemsAppProperty() {
			
			List<AppProperty> l = null;
			try {
				
				l = appPropertyService.findAllAppProperty();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
	
		
		
		@Autowired
		BBCService bBCService; 
		
		public List<BBC> autoCompleteBBC(String searchText) 
		{
			List<BBC> list=null;
			try {
				if(searchText==null || searchText.isEmpty())
				{
					list=bBCService.findAllBBC();
				}
				else
				{
					list=bBCService.findByDescriptionStartingWith(searchText);
				}
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
			}
			
			return list;
		}
		
		 public List<BBC> getSelectItemsBBC() {
			
			List<BBC> l = null;
			try {
				
				l = bBCService.findAllBBC();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
		
		
	@Autowired
	RegionService regionService; 
	
	public List<Region> autoCompleteRegion(String searchText) 
	{
		List<Region> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=regionService.findAllRegion();
			}
			else
			{
				list=regionService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Region> getSelectItemsRegion() {
		
		List<Region> l = null;
		try {
			
			l = regionService.findAllRegion();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	//===============DO NOT REMOVE THIS=================//
























	
	
	
	

}
