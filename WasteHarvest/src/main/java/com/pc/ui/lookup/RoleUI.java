package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.lookup.Role;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.RoleService;

@Component("roleUI")
@ViewScoped
public class RoleUI extends AbstractUI{

	@Autowired
	RoleService roleService;
	private ArrayList<Role> roleList;

	private Role role;

	@PostConstruct
	public void init() {
		
		try {
			super.prepareCurrentUser();
			role = new Role();
			roleList=(ArrayList<Role>) findAllRole();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}

	public void saveRole()
	{
		try {
			roleService.saveRole(role);
			displayInfoMssg("Update Successful...!!");
			roleList=(ArrayList<Role>) findAllRole();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteRole()
	{
		try {
			roleService.deleteRole(role);
			displayWarningMssg("Update Successful...!!");
			roleList=(ArrayList<Role>) findAllRole();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Role> findAllRole()
	{
		List<Role> list=new ArrayList<>();
		try {
			list= roleService.findAllRole();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Role> findAllRolePageable()
	{
		Pageable p=null;
		try {
			return roleService.findAllRole(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Role> findAllRoleSort()
	{
		Sort s=null;
		List<Role> list=new ArrayList<>();
		try {
			list =roleService.findAllRole(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		role = new Role();
	}
	

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public ArrayList<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(ArrayList<Role> roleList) {
		this.roleList =roleList;
	}

}
