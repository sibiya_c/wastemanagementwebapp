package com.pc.ui.lookup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.lookup.Category;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.CategoryService;

@Component
@ManagedBean(name="categoryUI")
@ViewScoped
public class CategoryUI extends AbstractUI{

	@Autowired
	CategoryService userService;
	private ArrayList<Category> categoryList;

	private Category category;

	@PostConstruct
	public void init() {
		category = new Category();
		categoryList=(ArrayList<Category>) findAllCategory();
		
	}

	public void saveCategory()
	{
		try {
			userService.saveCategory(category);
			displayInfoMssg("Update Successful...!!");
			categoryList=(ArrayList<Category>) findAllCategory();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteCategory()
	{
		try {
			userService.deleteCategory(category);
			displayWarningMssg("Update Successful...!!");
			categoryList=(ArrayList<Category>) findAllCategory();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Category> findAllCategory()
	{
		List<Category> list=new ArrayList<>();
		try {
			list= userService.findAllCategory();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Category> findAllCategoryPageable()
	{
		Pageable p=null;
		try {
			return userService.findAllCategory(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Category> findAllCategorySort()
	{
		Sort s=null;
		List<Category> list=new ArrayList<>();
		try {
			list =userService.findAllCategory(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		category = new Category();
	}
	

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public ArrayList<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(ArrayList<Category> categoryList) {
		this.categoryList =categoryList;
	}
	
   public void handleFileUpload(FileUploadEvent event) {	
		
		try {			
			userService.saveImage(category, event);
			displayInfoMssg("Image uploaded sucessfully...!!!");	
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}	
	}
	
	public StreamedContent getImgStreamedContent() throws Exception {
		byte[] image = null;
		try {
			if(category !=null && category.getImage() !=null)
			{
				image = category.getImage().getPic();
				return new DefaultStreamedContent(new ByteArrayInputStream(image),category.getImage().getType().trim()); 
			}
			else
			{
				return new DefaultStreamedContent();
			}
			
		} catch (Exception e) { 

			displayErrorMssg(e.getMessage());
			return new DefaultStreamedContent();
		} 
		
	}
	
	public StreamedContent getImageFromDB() throws Exception {
		prepareCurrentUser();
		FacesContext context = FacesContext.getCurrentInstance(); 
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else { 
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] image = null;
			try {
				image = currentUser.getImage().getPic();
			} catch (Exception e) { 
				displayErrorMssg(e.getMessage());
			} 
			return new DefaultStreamedContent(new ByteArrayInputStream(image),currentUser.getImage().getType().trim()); 
		}
	}

	

}
