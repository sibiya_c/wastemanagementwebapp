package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Region;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.RegionService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("regionUI")
@ViewScoped
public class RegionUI extends AbstractUI{

	@Autowired
	RegionService regionService;
	private ArrayList<Region> regionList;
	private Region region;
	private LazyDataModel<Region> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		region = new Region();
		loadRegionInfo();
	}

	public void saveRegion()
	{
		try {
			regionService.saveRegion(region);
			displayInfoMssg("Update Successful...!!");
			loadRegionInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteRegion()
	{
		try {
			regionService.deleteRegion(region);
			displayWarningMssg("Update Successful...!!");
			loadRegionInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Region> findAllRegion()
	{
		List<Region> list=new ArrayList<>();
		try {
			list= regionService.findAllRegion();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Region> findAllRegionPageable()
	{
		Pageable p=null;
		try {
			return regionService.findAllRegion(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Region> findAllRegionSort()
	{
		Sort s=null;
		List<Region> list=new ArrayList<>();
		try {
			list =regionService.findAllRegion(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		region = new Region();
	}
	
	public void loadRegionInfo()
	{
		 dataModel = new LazyDataModel<Region>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Region> list = new  ArrayList<Region>();
			   
			   @Override 
			   public List<Region> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Region>) entityDAOFacade.getResultList(Region.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Region.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Region obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Region getRowData(String rowKey) {
			        for(Region obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}
	
	public ArrayList<Region> getRegionList() {
		return regionList;
	}

	public void setRegionList(ArrayList<Region> regionList) {
		this.regionList =regionList;
	}
	
	public LazyDataModel<Region> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Region> dataModel) {
		this.dataModel = dataModel;
	}

}
