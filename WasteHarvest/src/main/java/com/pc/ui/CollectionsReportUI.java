package com.pc.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.beans.LabelTotalBean;
import com.pc.constants.AppConstants;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Item;
import com.pc.framework.AbstractUI;
import com.pc.service.CustomerProductService;
import com.pc.service.ReportService;
import com.pc.service.lookup.BBCService;
import com.pc.service.lookup.CategoryService;
import com.pc.service.lookup.ItemService;
import org.springframework.data.domain.Sort;

@Component("collectionsReportUI")
@ViewScoped
public class CollectionsReportUI extends AbstractUI{

  @Autowired
  ReportService reportService;
  @Autowired
  BBCService bbcService;
  @Autowired
  CategoryService categoryService;
  @Autowired
  ItemService itemService;
  
  @Autowired
  CustomerProductService customerProductService;
  
  List<LabelTotalBean> bbcTotalList;
  
  private List<BBC> bbcList;
  private List<Category> categoryList;
  private List<Item> itemList;
  
  private List<Integer> yearList=new ArrayList<>();
  
  private Integer year;
 
  
  
  @PostConstruct
  public void init() {
	  
		  try
		  {
			year=Integer.parseInt(AppConstants.sdfYYYY.format(new Date()));
			prepareCurrentUser();
			reloadGrapgData();
			populateYears();
			
		  }
		  catch (Exception e) {
				e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}
	
    public void reloadGrapgData() {
        try {
        	bbcList=bbcService.findAllBBC();
        	categoryList=categoryService.findAllCategory();
        	itemList=itemService.findAllItem( new Sort(Sort.Direction.ASC, "category"));
        	bbcTotalList=customerProductService.getBBCBean(year);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
    }
    
    public String getBackroundColor(Item item)
    {
    	String color="background-color:#"+item.getCategory().getColor()+";border:#FFFFFF;";
    	return color;
    }
    
    public String getBackroundColor(Category category)
    {
    	String color="background-color:#"+category.getColor()+";color:black";
    	return color;
    }
    
    public void clearYear()
    {
    	year=Integer.parseInt(AppConstants.sdfYYYY.format(new Date()));
    }
  
    public void populateYears()
    {
    	int currentYear=Integer.parseInt(AppConstants.sdfYYYY.format(new Date()));
    	for(int x=2018;x<=currentYear;x++)
    	{
    		yearList.add(x);
    	}
    }
    public Double calculateTotalPrice(BBC bbc,Item item)
    {
    	try {
    		Double totalPrice= reportService.calculateTotalPrice(bbc,item,year);
    		BigDecimal total=BigDecimal.ZERO;
    		if(bbc.getTotalPrice()==null)
    		{
    			total=new BigDecimal(totalPrice);
    			bbc.setTotalPrice(total);
    		}
    		else
    		{
    			total=new BigDecimal(totalPrice+bbc.getTotalPrice().doubleValue());
    			bbc.setTotalPrice(total);
    		}
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return null;
		}
    }
    
    public Double calculateTotalPrice(Item item)
    {
    	try {
    		Double totalPrice= reportService.calculateTotalPrice(item,year);
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return null;
		}
    }
    
    public Double calculateTotalPrice()
    {
    	try {
    		Double totalPrice= reportService.calculateTotalPrice(year);
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return null;
		}
    }
    
    
    private int getRandomAmount() {
        return (int) (Math.random() * 100000);
    }
 
   
	public List<BBC> getBbcList() {
		return bbcList;
	}

	public void setBbcList(List<BBC> bbcList) {
		this.bbcList = bbcList;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	public List<LabelTotalBean> getBbcTotalList() {
		return bbcTotalList;
	}

	public void setBbcTotalList(List<LabelTotalBean> bbcTotalList) {
		this.bbcTotalList = bbcTotalList;
	}

	public List<Integer> getYearList() {
		return yearList;
	}

	public void setYearList(List<Integer> yearList) {
		this.yearList = yearList;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
   
}
