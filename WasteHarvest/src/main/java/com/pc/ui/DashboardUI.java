package com.pc.ui;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.pc.entities.User;
import com.pc.framework.AbstractUI;
import com.pc.service.UserService;

@Component("dashboardUI")
@ViewScoped
public class DashboardUI extends AbstractUI{

  private String authUser="" ;
  private User currentUser;
  
  @Autowired
  UserService userService;
  @PostConstruct
  public void init() {
	  
	  try
	  {
		prepareCurrentUser();
    	authUser=showGreeting();
	  }
	  catch (Exception e) {
		displayErrorMssg(e.getMessage());
	}
  }
    
  public String showGreeting() {
   
    return "Hello " + currentUser.getName() + " "+currentUser.getSurname()+ "!";
  }
  
  public void prepareCurrentUser() throws Exception
  {
	  Authentication authentication =
		        SecurityContextHolder.getContext().getAuthentication();
	  currentUser= userService.findByEmail(authentication.getName() );
  }



public String getAuthUser() {
	return authUser;
}



public void setAuthUser(String authUser) {
	this.authUser = authUser;
}

public User getCurrentUser() {
	return currentUser;
}

public void setCurrentUser(User currentUser) {
	this.currentUser = currentUser;
}
}
