package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.MailLog;
import com.pc.framework.AbstractUI;
import com.pc.service.MailLogService;

@Component("mailLogUI")
@ViewScoped
public class MailLogUI extends AbstractUI{

	@Autowired
	MailLogService mailLogService;
	private ArrayList<MailLog> mailLogList;

	private MailLog mailLog;

	@PostConstruct
	public void init() {
		mailLog = new MailLog();
		mailLogList=(ArrayList<MailLog>) findAllMailLog();
	}

	public void saveMailLog()
	{
		try {
			mailLogService.saveMailLog(mailLog);
			displayInfoMssg("Update Successful...!!");
			mailLogList=(ArrayList<MailLog>) findAllMailLog();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteMailLog()
	{
		try {
			mailLogService.deleteMailLog(mailLog);
			displayWarningMssg("Update Successful...!!");
			mailLogList=(ArrayList<MailLog>) findAllMailLog();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<MailLog> findAllMailLog()
	{
		List<MailLog> list=new ArrayList<>();
		try {
			list= mailLogService.findAllMailLog();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<MailLog> findAllMailLogPageable()
	{
		Pageable p=null;
		try {
			return mailLogService.findAllMailLog(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<MailLog> findAllMailLogSort()
	{
		Sort s=null;
		List<MailLog> list=new ArrayList<>();
		try {
			list =mailLogService.findAllMailLog(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		mailLog = new MailLog();
	}
	

	public MailLog getMailLog() {
		return mailLog;
	}

	public void setMailLog(MailLog mailLog) {
		this.mailLog = mailLog;
	}
	
	public ArrayList<MailLog> getMailLogList() {
		return mailLogList;
	}

	public void setMailLogList(ArrayList<MailLog> mailLogList) {
		this.mailLogList =mailLogList;
	}

}
