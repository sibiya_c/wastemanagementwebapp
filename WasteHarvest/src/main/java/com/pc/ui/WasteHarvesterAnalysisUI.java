package com.pc.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.beans.LabelTotalBean;
import com.pc.beans.WasteHarvesterAnalysisBean;
import com.pc.constants.AppConstants;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Item;
import com.pc.framework.AbstractUI;
import com.pc.service.CustomerProductService;
import com.pc.service.ReportService;
import com.pc.service.lookup.BBCService;
import com.pc.service.lookup.CategoryService;
import com.pc.service.lookup.ItemService;

@Component("wasteHarvesterAnalysisUI")
@ViewScoped
public class WasteHarvesterAnalysisUI extends AbstractUI{

  @Autowired
  ReportService reportService;
  
  List<WasteHarvesterAnalysisBean> wasteHarvestList;
  List<LabelTotalBean> graphBean;
 
  @PostConstruct
  public void init() {
	  
		 try
		 {
			prepareCurrentUser();
			reloadGrapgData();
			
		 }catch (Exception e) {
				e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}
	
    public void reloadGrapgData() {
        try {
        	wasteHarvestList=reportService.findWasteHarvesterAnalysisBean();
        	graphBean=reportService.findRegionTotalBean();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
    }

	public List<WasteHarvesterAnalysisBean> getWasteHarvestList() {
		return wasteHarvestList;
	}

	public void setWasteHarvestList(List<WasteHarvesterAnalysisBean> wasteHarvestList) {
		this.wasteHarvestList = wasteHarvestList;
	}

	public List<LabelTotalBean> getGraphBean() {
		return graphBean;
	}

	public void setGraphBean(List<LabelTotalBean> graphBean) {
		this.graphBean = graphBean;
	}
    
  
   
}
