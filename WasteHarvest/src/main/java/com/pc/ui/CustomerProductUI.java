package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.CustomerProduct;
import com.pc.framework.AbstractUI;
import com.pc.service.CustomerProductService;

@Component("customerProductUI")
@ViewScoped
public class CustomerProductUI extends AbstractUI{

	@Autowired
	CustomerProductService customerProductService;
	private ArrayList<CustomerProduct> customerProductList;

	private CustomerProduct customerProduct;

	@PostConstruct
	public void init() {
		customerProduct = new CustomerProduct();
		customerProductList=(ArrayList<CustomerProduct>) findAllCustomerProduct();
	}

	public void saveCustomerProduct()
	{
		try {
			customerProductService.saveCustomerProduct(customerProduct);
			displayInfoMssg("Update Successful...!!");
			customerProductList=(ArrayList<CustomerProduct>) findAllCustomerProduct();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteCustomerProduct()
	{
		try {
			customerProductService.deleteCustomerProduct(customerProduct);
			displayWarningMssg("Update Successful...!!");
			customerProductList=(ArrayList<CustomerProduct>) findAllCustomerProduct();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<CustomerProduct> findAllCustomerProduct()
	{
		List<CustomerProduct> list=new ArrayList<>();
		try {
			list= customerProductService.findAllCustomerProduct();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<CustomerProduct> findAllCustomerProductPageable()
	{
		Pageable p=null;
		try {
			return customerProductService.findAllCustomerProduct(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<CustomerProduct> findAllCustomerProductSort()
	{
		Sort s=null;
		List<CustomerProduct> list=new ArrayList<>();
		try {
			list =customerProductService.findAllCustomerProduct(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		customerProduct = new CustomerProduct();
	}
	

	public CustomerProduct getCustomerProduct() {
		return customerProduct;
	}

	public void setCustomerProduct(CustomerProduct customerProduct) {
		this.customerProduct = customerProduct;
	}
	
	public ArrayList<CustomerProduct> getCustomerProductList() {
		return customerProductList;
	}

	public void setCustomerProductList(ArrayList<CustomerProduct> customerProductList) {
		this.customerProductList =customerProductList;
	}

}
