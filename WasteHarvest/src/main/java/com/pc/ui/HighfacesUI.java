package com.pc.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.beans.LabelTotalBean;
import com.pc.entities.CustomerProduct;
import com.pc.framework.AbstractUI;
import com.pc.model.Births;
import com.pc.service.CustomerProductService;

@Component("highfacesUI")
@ViewScoped
public class HighfacesUI  extends AbstractUI{
 
    private  Map<String, List<Births>> mappedList;
    
    private ArrayList<Births> boys = new ArrayList<>();
    private ArrayList<Births> girls = new ArrayList<>();
    
	@Autowired
    CustomerProductService customerProductService;
    List<LabelTotalBean> productCatList= new ArrayList<>();
    List<LabelTotalBean> itemsList= new ArrayList<>();
    
    @PostConstruct
    public void init() {
    	try {
			productCatList=customerProductService.getProductCategoryBean();
			itemsList=customerProductService.getItemBean();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
    }
 
    private void reloadInstitution() {
        try {
        	
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
       
    }
    
  
   
	

	public Map<String, List<Births>> getMappedList() {
		return mappedList;
	}

	public void setMappedList(Map<String, List<Births>> mappedList) {
		this.mappedList = mappedList;
	}

	public ArrayList<Births> getBoys() {
		return boys;
	}

	public void setBoys(ArrayList<Births> boys) {
		this.boys = boys;
	}

	public ArrayList<Births> getGirls() {
		return girls;
	}

	public void setGirls(ArrayList<Births> girls) {
		this.girls = girls;
	}

	public List<LabelTotalBean> getProductCatList() {
		return productCatList;
	}

	public void setProductCatList(List<LabelTotalBean> productCatList) {
		this.productCatList = productCatList;
	}

	public List<LabelTotalBean> getItemsList() {
		return itemsList;
	}

	public void setItemsList(List<LabelTotalBean> itemsList) {
		this.itemsList = itemsList;
	}

	


}