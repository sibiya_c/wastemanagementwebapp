package com.pc.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.beans.DateBean;
import com.pc.beans.LabelTotalBean;
import com.pc.constants.AppConstants;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Item;
import com.pc.framework.AbstractUI;
import com.pc.service.CustomerProductService;
import com.pc.service.ReportService;
import com.pc.service.lookup.BBCService;

@Component("wasteHarvesterPayoutUI")
@ViewScoped
public class WasteHarvesterPayoutUI extends AbstractUI{

  @Autowired
  ReportService reportService;
  
  @Autowired
  BBCService bbcService;

  @Autowired
  CustomerProductService customerProductService;
  
  private List<BBC> bbcList;
  
  private List<DateBean> dateList=new ArrayList<>();
  
  private Integer year;
  
  private List<Integer> yearList=new ArrayList<>();
  
  private List<LabelTotalBean> totalPerMonthList;
  
  List<LabelTotalBean> totalPerBBCList;
 
  
  
  @PostConstruct
  public void init() {
	  
		  try
		  {
			year=Integer.parseInt(AppConstants.sdfYYYY.format(new Date()));
			prepareCurrentUser();
			reloadGrapgData();
			populateYears();
		  }
		  catch (Exception e) {
				e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}
	
  public void populateTotalPerMonBean()
  {
	  try {
		totalPerMonthList=customerProductService.populateTotalPerMonBean(dateList);
	} catch (Exception e) {
		displayErrorMssg(e.getMessage());
		e.printStackTrace();
	}
  }
  
  public void populateTotalPerBBCBean()
  {
	  try {
		totalPerBBCList=customerProductService.populateTotalPerBBCBean(year);
	} catch (Exception e) {
		displayErrorMssg(e.getMessage());
		e.printStackTrace();
	}
  }
  public void populateYears()
  {
  	int currentYear=Integer.parseInt(AppConstants.sdfYYYY.format(new Date()));
  	for(int x=2018;x<=currentYear;x++)
  	{
  		yearList.add(x);
  	}
  }
  public void populateDate()
  {
	  dateList.clear();
	  for(int x=1;x<=12;x++)
	  {
		  DateBean dateBean=new DateBean(getStrMon(x)+"-"+year, x, year);
		  dateList.add(dateBean);
	  }
  }
  
    public void reloadGrapgData() {
        try {
        	populateDate();
        	populateTotalPerMonBean();
        	populateTotalPerBBCBean();
        	bbcList=bbcService.findAllBBC();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
    }
    
    public void clearYear()
    {
    	year=Integer.parseInt(AppConstants.sdfYYYY.format(new Date()));
    	reloadGrapgData();
    }
    
    public Double calculateTotalPrice(DateBean mon,BBC bbc)
    {
    	try {
    		Double totalPrice= reportService.calculateTotalPrice(bbc,mon);
    		
    		BigDecimal total=BigDecimal.ZERO;
    		if(mon.getTotalPrice()==null)
    		{
    			total=new BigDecimal(totalPrice);
    			mon.setTotalPrice(total);
    		}
    		else
    		{
    			total=new BigDecimal(totalPrice+mon.getTotalPrice().doubleValue());
    			mon.setTotalPrice(total);
    		}
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return null;
		}
    }
    
    public Double calculateTotalPrice(BBC bbc)
    {
    	try {
    		Double totalPrice= reportService.calculateTotalPrice(bbc, year);
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return null;
		}
    }
    
    public Double calculateMonthlyTotal()
    {
    	try {
    		Double totalPrice= reportService.calculateTotalPrice(year);
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return null;
		}
    }
    
	public List<BBC> getBbcList() {
		return bbcList;
	}

	public void setBbcList(List<BBC> bbcList) {
		this.bbcList = bbcList;
	}
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	public String getStrMon(int mon)
	{
		String strMon="";
		if(mon==1) {
			strMon="JAN";
		}else if(mon==2){
			strMon="FAB";
		}else if(mon==3){
			strMon="MAR";
		}else if(mon==4){
			strMon="APR";
		}else if(mon==5){
			strMon="MAY";
		}else if(mon==6){
			strMon="JUN";
		}else if(mon==7){
			strMon="JUL";
		}else if(mon==8){
			strMon="AUG";
		}else if(mon==9){
			strMon="SEP";
		}else if(mon==10){
			strMon="OCT";
		}else if(mon==11){
			strMon="NOV";
		}else if(mon==12){
			strMon="DEC";
		}
		
		return strMon;
	}

	public List<DateBean> getDateList() {
		return dateList;
	}

	public void setDateList(List<DateBean> dateList) {
		this.dateList = dateList;
	}
	

	public List<Integer> getYearList() {
		return yearList;
	}

	public void setYearList(List<Integer> yearList) {
		this.yearList = yearList;
	}

	public List<LabelTotalBean> getTotalPerMonthList() {
		return totalPerMonthList;
	}

	public void setTotalPerMonthList(List<LabelTotalBean> totalPerMonthList) {
		this.totalPerMonthList = totalPerMonthList;
	}

	public List<LabelTotalBean> getTotalPerBBCList() {
		return totalPerBBCList;
	}

	public void setTotalPerBBCList(List<LabelTotalBean> totalPerBBCList) {
		this.totalPerBBCList = totalPerBBCList;
	}
   
}
