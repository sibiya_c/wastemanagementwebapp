package com.pc.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.BuyingBackCenter;
import com.pc.entities.CustomerProduct;
import com.pc.entities.User;
import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Item;
import com.pc.framework.AbstractUI;
import com.pc.service.BuyingBackCenterService;
import com.pc.service.lookup.ItemService;

@Component("buyingBackCenterUI")
@ViewScoped
public class BuyingBackCenterUI extends AbstractUI{

	@Autowired
	BuyingBackCenterService buyingBackCenterService;
	
	@Autowired
	ItemService itemService;
	private ArrayList<BuyingBackCenter> buyingBackCenterList;

	private BuyingBackCenter buyingBackCenter;
	
	private User user;
	
	
	private CustomerProduct customerProduct;
	
	private Category category;
	
	private List<CustomerProduct> customerProductList=new ArrayList<>();
	
	private boolean continueAddingItems;

	@PostConstruct
	public void init() {
		try {
			prepareCurrentUser();
			buyingBackCenter = new BuyingBackCenter();
			buyingBackCenterList=(ArrayList<BuyingBackCenter>) findAllBuyingBackCenter();
			customerProduct=new CustomerProduct();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void continueAdding()
	{
		BigDecimal amountDue=BigDecimal.ZERO;
		for(CustomerProduct cp:customerProductList)
		{
			amountDue =new BigDecimal(cp.getTotalCost().longValueExact()+amountDue.longValueExact());
		}
		buyingBackCenter.setReceivedDate(new Date());
		buyingBackCenter.setAmountDue(amountDue);
		continueAddingItems=true;
	}
	
	public void prepareAddProduct()
	{
		continueAddingItems=false;
	}

	public void saveBuyingBackCenter() 
	{
		try {
			buyingBackCenterService.saveBuyingBackCenter(buyingBackCenter);
			displayInfoMssg("Update Successful...!!");
			buyingBackCenterList=(ArrayList<BuyingBackCenter>) findAllBuyingBackCenter();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	

	public void updatePayment() 
	{
		try {
			buyingBackCenterService.updatePayment(buyingBackCenter);
			displayInfoMssg("Update Successful...!!");
			buyingBackCenterList=(ArrayList<BuyingBackCenter>) findAllBuyingBackCenter();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void saveBuyingBackCenterDeatils()
	{
		try {
			
			buyingBackCenterService.saveBuyingBackCenterDeatils(buyingBackCenter,(ArrayList<CustomerProduct>) customerProductList,currentUser,user);
			displayInfoMssg("Update Successful...!!");
			buyingBackCenterList=(ArrayList<BuyingBackCenter>) findAllBuyingBackCenter();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteBuyingBackCenter()
	{
		try {
			buyingBackCenterService.deleteBuyingBackCenter(buyingBackCenter);
			displayWarningMssg("Update Successful...!!");
			buyingBackCenterList=(ArrayList<BuyingBackCenter>) findAllBuyingBackCenter();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<BuyingBackCenter> findAllBuyingBackCenter()
	{
		List<BuyingBackCenter> list=new ArrayList<>();
		try {
			list= buyingBackCenterService.findAllBuyingBackCenter();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<BuyingBackCenter> findAllBuyingBackCenterPageable()
	{
		Pageable p=null;
		try {
			return buyingBackCenterService.findAllBuyingBackCenter(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public void addCustomerProduct()
	{
		try {
			if(currentUser.getBbc() !=null){customerProduct.setBbc(currentUser.getBbc());}
			calculateTotalCost(customerProduct);
			customerProductList.add(customerProduct);
			clearProduct();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void calculateTotalCost(CustomerProduct customerProduct) throws Exception
	{
		BigDecimal total=new BigDecimal(customerProduct.getItem().getPricePerKg()*customerProduct.getWeightInKG());
		customerProduct.setPricePerKG(new BigDecimal(customerProduct.getItem().getPricePerKg()));
		customerProduct.setTotalCost(total);
		
	}
	
	public void removeCustomerProduct()
	{
		try {
			customerProductList.remove(customerProduct);
			clearProduct();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void prepareUpdateCustomerProduct()
	{
		try {
			customerProductList.remove(customerProduct);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void clearProduct()
	{
		category=null;
		customerProduct=new CustomerProduct();
	}
	public List<BuyingBackCenter> findAllBuyingBackCenterSort()
	{
		Sort s=null;
		List<BuyingBackCenter> list=new ArrayList<>();
		try {
			list =buyingBackCenterService.findAllBuyingBackCenter(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		clearProduct();
		user=null;
		customerProductList.clear();
		buyingBackCenter=new BuyingBackCenter();
		prepareAddProduct();
	}
	
	 public List<Item> getSelectItemsItem() {
		
		List<Item> l = null;
		try {
			
			l = itemService.findByCategory(category);
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	

	public BuyingBackCenter getBuyingBackCenter() {
		return buyingBackCenter;
	}

	public void setBuyingBackCenter(BuyingBackCenter buyingBackCenter) {
		this.buyingBackCenter = buyingBackCenter;
	}
	
	public ArrayList<BuyingBackCenter> getBuyingBackCenterList() {
		return buyingBackCenterList;
	}

	public void setBuyingBackCenterList(ArrayList<BuyingBackCenter> buyingBackCenterList) {
		this.buyingBackCenterList =buyingBackCenterList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	public CustomerProduct getCustomerProduct() {
		return customerProduct;
	}

	public void setCustomerProduct(CustomerProduct customerProduct) {
		this.customerProduct = customerProduct;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<CustomerProduct> getCustomerProductList() {
		return customerProductList;
	}

	public void setCustomerProductList(List<CustomerProduct> customerProductList) {
		this.customerProductList = customerProductList;
	}

	public boolean isContinueAddingItems() {
		return continueAddingItems;
	}

	public void setContinueAddingItems(boolean continueAddingItems) {
		this.continueAddingItems = continueAddingItems;
	}

}
