package com.pc.beans;

import java.math.BigDecimal;

public class LabelTotalBean {
 
    public String label;
    public Long total;
    
	public LabelTotalBean() {
	
	}
	
	


	public LabelTotalBean(String label, Long total) {
		if(label==null){label="N/A";}
		if(total==null){total=0L;}
		this.label = label;
		this.total = total;
	}
	
	public LabelTotalBean(String label, BigDecimal total) {
		if(label==null){label="N/A";}
		if(total==null){total=BigDecimal.ZERO;}
		this.label = label;
		this.total = total.longValue();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}



	public Long getTotal() {
		return total;
	}



	public void setTotal(Long total) {
		this.total = total;
	}



	
	
    
}