package com.pc.beans;

public class WasteHarvesterAnalysisBean {
	
	private String regionDesc;
	private String bbcDesc;
	private Integer totalHaverster;
	private Integer totalPerRegion;
	private String strTotalPerRegion;
	
	public WasteHarvesterAnalysisBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WasteHarvesterAnalysisBean(String regionDesc, String bbcDesc, Integer totalHaverster,
			Integer totalPerRegion) {
		super();
		this.regionDesc = regionDesc;
		this.bbcDesc = bbcDesc;
		this.totalHaverster = totalHaverster;
		this.totalPerRegion = totalPerRegion;
	}
	public String getRegionDesc() {
		return regionDesc;
	}
	public void setRegionDesc(String regionDesc) {
		this.regionDesc = regionDesc;
	}
	public String getBbcDesc() {
		return bbcDesc;
	}
	public void setBbcDesc(String bbcDesc) {
		this.bbcDesc = bbcDesc;
	}
	public Integer getTotalHaverster() {
		return totalHaverster;
	}
	public void setTotalHaverster(Integer totalHaverster) {
		this.totalHaverster = totalHaverster;
	}
	public Integer getTotalPerRegion() {
		return totalPerRegion;
	}
	public void setTotalPerRegion(Integer totalPerRegion) {
		this.totalPerRegion = totalPerRegion;
	}
	public String getStrTotalPerRegion() {
		return strTotalPerRegion;
	}
	public void setStrTotalPerRegion(String strTotalPerRegion) {
		this.strTotalPerRegion = strTotalPerRegion;
	}
	

}
