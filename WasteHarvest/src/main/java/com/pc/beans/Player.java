package com.pc.beans;

import java.util.Map;

public class Player {
	private String name;
	Map<Integer,Integer> goals;
	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Player(String name, Map<Integer, Integer> goals) {
		super();
		this.name = name;
		this.goals = goals;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<Integer, Integer> getGoals() {
		return goals;
	}
	
	public Integer getGoals(Integer year) {
		return goals.get(year);
	}
	
	
	public void setGoals(Map<Integer, Integer> goals) {
		this.goals = goals;
	}
}
