package com.pc.beans;

public class EmploymentBean {

	private String description;
	private Integer numOfEmp;
	private Integer totalMale;
	private Integer totalFemale;
	
	public EmploymentBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmploymentBean(String desc, Integer numOfEmp, Integer totalMale, Integer totalFemale) {
		super();
		this.description = desc;
		this.numOfEmp = numOfEmp;
		this.totalMale = totalMale;
		this.totalFemale = totalFemale;
	}
	
	public Integer getNumOfEmp() {
		return numOfEmp;
	}
	public void setNumOfEmp(Integer numOfEmp) {
		this.numOfEmp = numOfEmp;
	}
	public Integer getTotalMale() {
		return totalMale;
	}
	public void setTotalMale(Integer totalMale) {
		this.totalMale = totalMale;
	}
	public Integer getTotalFemale() {
		return totalFemale;
	}
	public void setTotalFemale(Integer totalFemale) {
		this.totalFemale = totalFemale;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
