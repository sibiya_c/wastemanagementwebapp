package com.pc.beans;

import java.math.BigDecimal;

import javax.persistence.Transient;

public class DateBean {
	private String displayName;
	private Integer month;
	private Integer year;
	private BigDecimal totalPrice;
	
	public DateBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DateBean(String displayName, Integer month, Integer year) {
		super();
		this.displayName = displayName;
		this.month = month;
		this.year = year;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
}
