package com.pc.framework;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.pc.entities.User;
import com.pc.service.UserService;


public abstract class AbstractService implements Serializable
{
	@Autowired
	UserService userService;
	
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@PostConstruct
	public void init() {
		  
		  try
		  {
			
		  }
		  catch (Exception e) {
			
		  }
	}
	
	
	
	
	public User getCurrentUser() throws Exception
	{
		User currentUser=null;
		
		  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		  if(authentication !=null && authentication.getName() !=null){
			  currentUser = userService.findByEmail(authentication.getName() );
		  }
		  
		  return currentUser;
		  
	}
	
	
	
	
	
	
	
	
	


}
