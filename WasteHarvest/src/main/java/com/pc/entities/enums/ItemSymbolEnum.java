package com.pc.entities.enums;

public enum ItemSymbolEnum {

  Symbol_1("PET"), 
  Symbol_2("Rejected"),
  Symbol_3("Pending Approval"),
  Symbol_4("Active"),
  Symbol_5("In-Active") ,
  Symbol_6("Email not confrimed") ;

  private String displayName;

  private ItemSymbolEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
