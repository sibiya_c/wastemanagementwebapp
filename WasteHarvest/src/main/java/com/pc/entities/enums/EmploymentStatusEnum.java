package com.pc.entities.enums;

public enum EmploymentStatusEnum {

  FullTime ("Full Time"), 
  PartTime ("Part Time"),
  Terminated ("Terminated");


  private String displayName;

  private EmploymentStatusEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
