package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

import com.pc.beans.DateBean;
import com.pc.beans.LabelTotalBean;
import com.pc.constants.AppConstants;
import com.pc.entities.CustomerProduct;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Item;
import com.pc.framework.AbstractService;
import com.pc.repositories.CustomerProductRepository;
import com.pc.service.lookup.BBCService;

@Service
public class CustomerProductService extends AbstractService{
	@Autowired
	CustomerProductRepository repository;
	@Autowired
	BBCService bbcService;
	
	public void saveCustomerProduct(CustomerProduct customerProduct)  throws Exception
	{
	    if(customerProduct.getId()==null)
		{
			customerProduct.setCreateDate(new Date());
		}
	    else{
			if(getCurrentUser() !=null){customerProduct.setLastUpdateUser(getCurrentUser());}
			customerProduct.setLastUpdateDate(new Date());
		}
		repository.save(customerProduct);
	}
	
	public void deleteCustomerProduct(CustomerProduct customerProduct)  throws Exception
	{
		repository.delete(customerProduct);
	}
	
	public void deleteCustomerProductByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<CustomerProduct> findAllCustomerProduct()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<CustomerProduct> findAllCustomerProduct(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<CustomerProduct> findAllCustomerProduct(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public CustomerProduct findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public void deleteAllCustomerProduct(List<CustomerProduct> list)  throws Exception
	{
		repository.deleteAll(list);
	}
	
	public void saveAllCustomerProduct(List<CustomerProduct> list)  throws Exception
	{
		repository.saveAll(list);
	}
	
	public List<CustomerProduct> findByParentCustomerProduct(CustomerProduct parentCustomerProduct) throws Exception
	{
		return repository.findByParentCustomerProduct(parentCustomerProduct);
	}
	
	public List<LabelTotalBean> getItemBean()throws Exception
	{
		return repository.getItemBean();
	}
	
	
	public List<LabelTotalBean> getProductCategoryBean()throws Exception
	{
		return repository.getProductCategoryBean();
	}

	public Double calculateTotalPrice(BBC bbc, Item item,int year) throws Exception{
		Long total= repository.calculateTotalPrice(bbc,item,year);
		if(total==null){
			total=0L;
		}
		return total.doubleValue();
	}
	
	public Double calculateTotalPrice(Item item,int year) throws Exception{
		Long total= repository.calculateTotalPrice(item,year);
		if(total==null){
			total=0L;
		}
		return total.doubleValue();
	}
	
	public Double calculateTotalPrice(BBC bbc,int year) throws Exception{
		Long total= repository.calculateTotalPrice(bbc,year);
		if(total==null){
			total=0L;
		}
		return total.doubleValue();
	}
	
	public Double calculateTotalPrice(int year) throws Exception{
		Long total= repository.calculateTotalPrice(year);
		if(total==null){
			total=0L;
		}
		return total.doubleValue();
	}
	
	public Double calculateTotalPrice(BBC bbc, DateBean mon) throws Exception{
		Long total= repository.calculateTotalPrice(bbc,mon.getMonth(),mon.getYear());
		if(total==null){
			total=0L;
		}
		return total.doubleValue();
	}
	
	
	public List<LabelTotalBean> getBBCBean(int year)throws Exception
	{
		return repository.getBBCBean(year);
	}

	public List<LabelTotalBean> populateTotalPerMonBean(List<DateBean> dateList)throws Exception {
		List<LabelTotalBean> list=new ArrayList<LabelTotalBean>();
		
		for(DateBean mon:dateList)
		{
			Long total= repository.calculateTotalPrice(mon.getMonth(),mon.getYear());
			if(total==null){
				total=0L;
			}
			LabelTotalBean labelTotalBean=new LabelTotalBean(mon.getDisplayName(), total);
			list.add(labelTotalBean);
		}
		return list;
	}
	
	public List<LabelTotalBean> populateTotalPerBBCBean(int year)throws Exception {
		List<LabelTotalBean> list=new ArrayList<LabelTotalBean>();
		List<BBC> allBBC=bbcService.findAllBBC();
		for(BBC bbc:allBBC)
		{
			Long total= repository.calculateTotalPrice(bbc,year);
			if(total==null){
				total=0L;
			}
			LabelTotalBean labelTotalBean=new LabelTotalBean(bbc.getDescription(), total);
			list.add(labelTotalBean);
		}
		return list;
	}
	
	
	/*public List<CustomerProduct> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
