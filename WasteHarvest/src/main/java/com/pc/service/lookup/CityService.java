package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.City;
import com.pc.entities.lookup.Province;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.CityRepository;

@Service
public class CityService extends AbstractService{
	@Autowired
	CityRepository repository;
	
	public void saveCity(City city)  throws Exception
	{
	    if(city.getId()==null)
		{
			city.setCreateDate(new Date());
		}
	    else{
			if(getCurrentUser() !=null){city.setLastUpdateUser(getCurrentUser());}
			city.setLastUpdateDate(new Date());
		}
		repository.save(city);
	}
	
	public void deleteCity(City city)  throws Exception
	{
		repository.delete(city);
	}
	
	public void deleteCityByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<City> findAllCity()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<City> findAllCity(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<City> findAllCity(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public City findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<City> findByProvince(Province province)
	{
		return repository.findByProvince(province);
	}
	
	

}
