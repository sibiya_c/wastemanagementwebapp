package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Municipality;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.MunicipalityRepository;

@Service
public class MunicipalityService extends AbstractService{
	@Autowired
	MunicipalityRepository repository;
	
	public void saveMunicipality(Municipality municipality)  throws Exception
	{
	    if(municipality.getId()==null)
		{
			municipality.setCreateDate(new Date());
		}
	    else{
			if(getCurrentUser() !=null){municipality.setLastUpdateUser(getCurrentUser());}
			municipality.setLastUpdateDate(new Date());
		}
		repository.save(municipality);
	}
	
	public void deleteMunicipality(Municipality municipality)  throws Exception
	{
		repository.delete(municipality);
	}
	
	public void deleteMunicipalityByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Municipality> findAllMunicipality()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Municipality> findAllMunicipality(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Municipality> findAllMunicipality(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Municipality findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Municipality> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	
	

}
