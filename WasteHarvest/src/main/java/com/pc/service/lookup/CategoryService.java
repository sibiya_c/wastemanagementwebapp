package com.pc.service.lookup;

import java.util.List;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.Date;

import com.pc.entities.ImageModel;
import com.pc.entities.lookup.Category;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.CategoryRepository;
import com.pc.service.ImagesService;

@Service
public class CategoryService extends AbstractService{
	@Autowired
	CategoryRepository repository;
	

	@Autowired
	ImagesService imagesService;
	
	public void saveCategory(Category category)  throws Exception
	{
	    if(category.getId()==null)
		{
			category.setCreateDate(new Date());
		}
	    else{
			if(getCurrentUser() !=null){category.setLastUpdateUser(getCurrentUser());}
			category.setLastUpdateDate(new Date());
		}
		repository.save(category);
	}
	
	public void saveCategory(Category category, ImageModel imageModel)  throws Exception
	{
	    if(category.getId()==null)
		{
	    	category.setCreateDate(new Date());
			imageModel  = imagesService.save(imageModel);
			category.setImage(imageModel);
			repository.save(category);
		}
	    else
	    {
	    	ImageModel newImg=category.getImage();
	    	if(category.getImage()==null)
	    	{
	    		imageModel  = imagesService.save(imageModel);
				category.setImage(imageModel);
				repository.save(category);
	    	}
	    	else
	    	{
				newImg.setName(imageModel.getName());
				newImg.setType(imageModel.getType());
				newImg.setPic(imageModel.getPic());
				category.setImage(newImg);
				repository.save(category);
	    	}
	    }
	    
		
	}
	
	public void deleteCategory(Category category)  throws Exception
	{
		repository.delete(category);
	}
	
	public void deleteCategoryByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Category> findAllCategory()  throws Exception
	{
		List<Category> list=repository.findAll();
		return list;
	}
	
	public List<Category> findAllCategoryWithImg()  throws Exception
	{
		List<Category> list=repository.findAll();
		for(Category category:list)
		{
			byte[] image = null;
			if(category !=null && category.getImage() !=null)
			{
				image = category.getImage().getPic();
				category.setStreamedContent( new DefaultStreamedContent(new ByteArrayInputStream(image),category.getImage().getType().trim())); 
			}
			else
			{
				category.setStreamedContent(new DefaultStreamedContent());
			}
		}
		return list;
	}
	
	public Page<Category> findAllCategory(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Category> findAllCategory(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Category findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}

	public void saveImage(Category category, FileUploadEvent event) throws Exception {
		ImageModel imageModel =  new ImageModel();
		imageModel.setName(event.getFile().getFileName().trim());
		imageModel.setType(event.getFile().getContentType());
		imageModel.setPic(event.getFile().getContents());
		imageModel  = imagesService.save(imageModel);
		category.setImage(imageModel);
		saveCategory(category);
		
	}
	
	

}
