package com.pc.service.lookup;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.lookup.Gender;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.GenderRepository;

@Service
public class GenderService extends AbstractService{
	@Autowired
	GenderRepository repository;
	
	public void saveGender(Gender gender) throws Exception
	{
		if(gender.getId()==null)
		{
			gender.setCreateDate(new Date());
			
		}else{
			if(getCurrentUser() !=null){gender.setLastUpdateUser(getCurrentUser());}
			gender.setLastUpdateDate(new Date());
		}
		
		repository.saveAndFlush(gender);
		
	}
	
	public Gender findByGenderName(String name) throws Exception {
		return repository.findByGenderName(name);
	}
	
	public void deleteGender(Gender gender) throws Exception
	{
		repository.delete(gender);
	}
	
	/*public void deleteGenderByID(Integer  arg0)
	{
		 repository.delete(arg0);
	}
	*/
	public List<Gender> findAllGender() throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Gender> findAllGender(Pageable p) throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Gender> findAllGender(Sort s) throws Exception
	{
		return repository.findAll(s);
	}



	public Gender findById(Long parseLong)  throws Exception{
		return repository.findById(parseLong);
	}
	
	/*public Gender findOnelGender(Integer  arg0)
	{
		return repository.findOne(arg0);
	}*/
	

}
