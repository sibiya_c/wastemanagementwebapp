package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.Address;
import com.pc.entities.lookup.DumpSite;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.DumpSiteRepository;
import com.pc.service.AddressService;

@Service
public class DumpSiteService extends AbstractService{
	@Autowired
	DumpSiteRepository repository;
	
	@Autowired
	AddressService addressService;
	
	public void saveDumpSite(DumpSite dumpSite, Address address)  throws Exception
	{
	    if(dumpSite.getId()==null)
		{
			dumpSite.setCreateDate(new Date());
		}
	    else{
			if(getCurrentUser() !=null){dumpSite.setLastUpdateUser(getCurrentUser());}
			dumpSite.setLastUpdateDate(new Date());
		}
	    addressService.saveAddress(address);
	    dumpSite.setResidentialAddress(address);
		repository.save(dumpSite);
	}
	
	public void deleteDumpSite(DumpSite dumpSite)  throws Exception
	{
		Address address=dumpSite.getResidentialAddress();
		repository.delete(dumpSite);
		addressService.deleteAddress(address);
	}
	
	public void deleteDumpSiteByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<DumpSite> findAllDumpSite()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<DumpSite> findAllDumpSite(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<DumpSite> findAllDumpSite(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public DumpSite findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	

}
