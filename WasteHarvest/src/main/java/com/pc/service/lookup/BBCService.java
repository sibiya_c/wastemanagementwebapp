package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.Address;
import com.pc.entities.lookup.BBC;
import com.pc.framework.AbstractService;
import com.pc.repositories.lookup.BBCRepository;
import com.pc.service.AddressService;

@Service
public class BBCService extends AbstractService{
	@Autowired
	BBCRepository repository;
	
	@Autowired
	AddressService addressService;
	
	public void saveBBC(BBC bBC, Address address)  throws Exception
	{
	    if(bBC.getId()==null)
		{
			bBC.setCreateDate(new Date());
		}else{
			if(getCurrentUser() !=null){bBC.setLastUpdateUser(getCurrentUser());}
			bBC.setLastUpdateDate(new Date());
		}
	    addressService.saveAddress(address);
	    bBC.setResidentialAddress(address);
		repository.save(bBC);
	}
	
	public void deleteBBC(BBC bBC)  throws Exception
	{
		repository.delete(bBC);
	}
	
	public void deleteBBCByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<BBC> findAllBBC()  throws Exception
	{
		return repository.findAll();
	}
	
	public List<BBC> findAllByOrderByDescriptionAsc()  throws Exception
	{
		return repository.findAllByOrderByDescriptionAsc();
	}
	
	public List<BBC> findAllByOrderByRegionAsc()  throws Exception
	{
		return repository.findAllByOrderByRegionAsc();
	}

	
	public Page<BBC> findAllBBC(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<BBC> findAllBBC(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public BBC findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<BBC> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	
	

}
