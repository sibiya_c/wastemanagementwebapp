package com.pc.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.Address;
import com.pc.framework.AbstractService;
import com.pc.repositories.AddressRepository;

@Service
public class AddressService extends AbstractService{
	@Autowired
	AddressRepository repository;
	
	public void saveAddress(Address address) throws Exception
	{
		/*
		 * if(address.getAddressLine4()==null){address.setAddressLine4("");}
		 * if(address.getCode()==null){address.setCode("");}
		 */
		if(address.getId()==null)
		{
			address.setCreateDate(new Date());
		}
		else{
			if(getCurrentUser() !=null){address.setLastUpdateUser(getCurrentUser());}
			address.setLastUpdateDate(new Date());
		}
		repository.save(address);
	}
	
	public void deleteAddress(Address address) throws Exception
	{
		repository.delete(address);
	}
	
	public void deleteAddressByID(Integer  arg0) throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Address> findAllAddress() throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Address> findAllAddress(Pageable p) throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Address> findAllAddress(Sort s) throws Exception
	{
		return repository.findAll(s);
	}
	
		
	public Optional<Address> findAddressById(Integer  arg0) throws Exception
	{
		return repository.findById(arg0);
	}

	

}
