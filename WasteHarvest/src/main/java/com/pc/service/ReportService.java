package com.pc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pc.beans.DateBean;
import com.pc.beans.EmploymentBean;
import com.pc.beans.LabelTotalBean;
import com.pc.beans.WasteHarvesterAnalysisBean;
import com.pc.entities.enums.EmploymentStatusEnum;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Item;
import com.pc.entities.lookup.Role;
import com.pc.framework.AbstractService;
import com.pc.service.lookup.BBCService;
import com.pc.service.lookup.GenderService;
import com.pc.service.lookup.RoleService;

@Service
public class ReportService extends AbstractService{
	@Autowired
	BBCService bbcService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	GenderService genderService;
	
	@Autowired
	CustomerProductService customerProductService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	RoleService roleService;
	
	public List<WasteHarvesterAnalysisBean> findWasteHarvesterAnalysisBean() throws Exception
	{
		List<WasteHarvesterAnalysisBean> list=new ArrayList<>();
		List<BBC> allBBC=bbcService.findAllByOrderByRegionAsc();
		for(BBC bbc:allBBC)
		{
			WasteHarvesterAnalysisBean bean=new WasteHarvesterAnalysisBean();
			bean.setRegionDesc(bbc.getRegion().getDescription());
			bean.setBbcDesc(bbc.getDescription());
			bean.setTotalHaverster(userService.countByBBC(bbc));
			bean.setTotalPerRegion(userService.countByRegion(bbc.getRegion()));
			bean.setStrTotalPerRegion(bbc.getRegion().getCode()+": "+bean.getTotalPerRegion());
			
			list.add(bean);
		}
		
		return list;
		
	}
	
	public List<LabelTotalBean> findRegionTotalBean() throws Exception
	{
		List<LabelTotalBean> regionTotalList=userService.getILabelTotalBeanByRegion();
		return regionTotalList;
	}
	
	public List<EmploymentBean> findEmploymentStatusReport(EmploymentStatusEnum status) throws Exception
	{
		List<EmploymentBean>  list=new ArrayList<>();
		
		List<BBC> allBBC=bbcService.findAllBBC();
		
		for(BBC bbc:allBBC)
		{
			if(status==null)
			{
				EmploymentBean employmentBean=new EmploymentBean(bbc.getDescription(), userService.countByBBC(bbc), userService.countByBBCAndGender(bbc,genderService.findByGenderName("Male")), userService.countByBBCAndGender(bbc,genderService.findByGenderName("Female")));
				list.add(employmentBean);
			}
			else
			{
				EmploymentBean employmentBean=new EmploymentBean(bbc.getDescription(), userService.countByBBCAndEmpStatus(bbc,status), userService.countByBBCAndGenderEmpStatus(bbc,genderService.findByGenderName("Male"),status), userService.countByBBCAndGenderEmpStatus(bbc,genderService.findByGenderName("Female"),status));
				list.add(employmentBean);
			}
		}
		
		return list;
	}
	
	public List<EmploymentBean> findEmploymentStatusPerRoleReport(EmploymentStatusEnum status,BBC bbc) throws Exception
	{
		List<EmploymentBean>  list=new ArrayList<>();
		
		List<Role> allRoles=roleService.findAllRole();
		
		for(Role role:allRoles)
		{
			if(status==null)
			{
				EmploymentBean employmentBean=new EmploymentBean(role.getDescription(), userRoleService.countByRoleAndBBC(role,bbc), userRoleService.countByRoleAndGenderAndBBC(role,genderService.findByGenderName("Male"),bbc), userRoleService.countByRoleAndGenderAndBBC(role,genderService.findByGenderName("Female"),bbc));
				list.add(employmentBean);
			}
			else
			{
				EmploymentBean employmentBean=new EmploymentBean(role.getDescription(), userRoleService.countByRoleAndEmpStatusAndBBC(role,status,bbc), userRoleService.countByRoleAndGenderEmpStatusAndBBC(role,genderService.findByGenderName("Male"),status,bbc), userRoleService.countByRoleAndGenderEmpStatusAndBBC(role,genderService.findByGenderName("Female"),status,bbc));
				list.add(employmentBean);
			}
		}
		
		return list;
	}

	public Double calculateTotalPrice(BBC bbc, Item item,int year)throws Exception {
		return customerProductService.calculateTotalPrice(bbc,item,year);
	}
	
	public Double calculateTotalPrice(Item item,int year)throws Exception {
		return customerProductService.calculateTotalPrice(item,year);
	}
	
	public Double calculateTotalPrice(BBC bbc, DateBean mon)throws Exception {
		return customerProductService.calculateTotalPrice(bbc,mon);
	}

	public Double calculateTotalPrice(BBC bbc, Integer year)throws Exception {
		return customerProductService.calculateTotalPrice(bbc,year);
	}
	
	public Double calculateTotalPrice(Integer year)throws Exception {
		return customerProductService.calculateTotalPrice(year);
	}
	
	
	
	

}
