package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.BankingDetails;
import com.pc.entities.User;
import com.pc.framework.AbstractService;
import com.pc.repositories.BankingDetailsRepository;

@Service
public class BankingDetailsService extends AbstractService{
	@Autowired
	BankingDetailsRepository repository;
	
	@Autowired
	UserService userService;
	
	public void saveBankingDetails(BankingDetails bankingDetails)  throws Exception
	{
	    if(bankingDetails.getId()==null)
		{
	    	bankingDetails.setDeleted(false);
			bankingDetails.setCreateDate(new Date());
		}
	    else{
			if(getCurrentUser() !=null){bankingDetails.setLastUpdateUser(getCurrentUser());}
			bankingDetails.setLastUpdateDate(new Date());
		}
		repository.save(bankingDetails);
	}
	
	public void saveBankingDetails(BankingDetails bankingDetails,User user)  throws Exception
	{
	    if(bankingDetails.getId()==null)
		{
	    	bankingDetails.setDeleted(false);
			bankingDetails.setCreateDate(new Date());
			user.setBankingDetails(bankingDetails);
			repository.save(bankingDetails);
			userService.saveUser(user);
		}
	    else
	    {
	    	repository.save(bankingDetails);
	    }
	  
	    
	}
	
	public void deleteBankingDetails(BankingDetails bankingDetails)  throws Exception
	{
		repository.delete(bankingDetails);
	}
	
	public void deleteBankingDetailsByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<BankingDetails> findAllBankingDetails()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<BankingDetails> findAllBankingDetails(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<BankingDetails> findAllBankingDetails(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public BankingDetails findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	/*public List<BankingDetails> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
