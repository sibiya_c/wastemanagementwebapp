package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.DocConfigDetailActionHist;

@Repository
public interface DocConfigDetailActionHistRepository extends JpaRepository<DocConfigDetailActionHist, Integer> 
{
	
	public DocConfigDetailActionHist findById(Long parseLong);
	//public List<DocConfigDetailActionHist> findByDescriptionStartingWith(String description);
}
