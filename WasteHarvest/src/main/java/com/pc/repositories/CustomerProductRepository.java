package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pc.beans.LabelTotalBean;
import com.pc.entities.CustomerProduct;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Item;

@Repository
public interface CustomerProductRepository extends JpaRepository<CustomerProduct, Integer> 
{
	
	public CustomerProduct findById(Long parseLong);
	public List<CustomerProduct> findByParentCustomerProduct(CustomerProduct parentCustomerProduct);
	//public List<CustomerProduct> findByDescriptionStartingWith(String description);
	@Query("SELECT new com.pc.beans.LabelTotalBean(o.item.description, SUM(o.weightInKG)) "
			+ "FROM CustomerProduct o GROUP BY o.item.description")
	public List<LabelTotalBean> getItemBean();
	
	@Query("SELECT new com.pc.beans.LabelTotalBean(o.item.category.title, SUM(o.weightInKG)) "
			+ "FROM CustomerProduct o GROUP BY o.item.category.title")
	public List<LabelTotalBean> getProductCategoryBean();
	
	@Query("SELECT SUM(o.totalCost) "
			+ "FROM CustomerProduct o WHERE o.bbc = ?1 AND o.item = ?2 AND YEAR(o.createDate) = ?3")
	public Long calculateTotalPrice(BBC bbc,Item item,int year);
	
	@Query("SELECT SUM(o.totalCost) "
			+ "FROM CustomerProduct o WHERE o.item = ?1 AND YEAR(o.createDate) = ?2")
	public Long calculateTotalPrice(Item item,int year);
	
	
	@Query("SELECT SUM(o.totalCost) "
			+ "FROM CustomerProduct o WHERE o.bbc = ?1 AND YEAR(o.createDate) = ?2")
	public Long calculateTotalPrice(BBC bbc,int year);
	
	@Query("SELECT SUM(o.totalCost) "
			+ "FROM CustomerProduct o WHERE YEAR(o.createDate) = ?1")
	public Long calculateTotalPrice(int year);
	
	/*
	 * @Query("SELECT new com.pc.beans.LabelTotalBean(o.bbc.description, SUM(o.totalCost)) "
	 * +
	 * "FROM CustomerProduct o WHERE YEAR(o.createDate) = ?1 GROUP BY o.bbc.description"
	 * )
	 */
	@Query("SELECT new com.pc.beans.LabelTotalBean(o.bbc.description, SUM(o.totalCost)) "
			+ "FROM CustomerProduct o WHERE YEAR(o.createDate) = ?1 GROUP BY o.bbc.description")
	public List<LabelTotalBean> getBBCBean(int year);
	
	@Query("SELECT SUM(o.totalCost) "
			+ "FROM CustomerProduct o WHERE o.bbc = ?1 AND MONTH(o.createDate) = ?2 AND YEAR(o.createDate) = ?3")
	public Long calculateTotalPrice(BBC bbc, Integer month, Integer year);
	
	@Query("SELECT SUM(o.totalCost) "
			+ "FROM CustomerProduct o WHERE MONTH(o.createDate) = ?1 AND YEAR(o.createDate) = ?2")
	public Long calculateTotalPrice(Integer month, Integer year);
	
}
