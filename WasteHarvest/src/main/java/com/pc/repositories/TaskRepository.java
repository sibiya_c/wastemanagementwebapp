package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> 
{
	
	public Task findById(Long parseLong);
	//public List<Task> findByDescriptionStartingWith(String description);
}
