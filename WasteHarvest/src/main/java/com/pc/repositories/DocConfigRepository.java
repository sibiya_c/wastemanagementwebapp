package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.DocConfig;
import com.pc.entities.lookup.DocumentLevel;
import com.pc.entities.lookup.Workflow;

@Repository
public interface DocConfigRepository extends JpaRepository<DocConfig, Integer> 
{
	
	public DocConfig findById(Long parseLong);
	public List<DocConfig> findByWorkflowAndDocumentLevelOrderByDescriptionAsc(Workflow workflow,DocumentLevel documentLevel);
	//public List<DocConfig> findByDescriptionStartingWith(String description);
}
