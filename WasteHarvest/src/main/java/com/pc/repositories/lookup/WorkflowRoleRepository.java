package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pc.entities.enums.ApprovalStatusEnum;
import com.pc.entities.enums.PermissionEnum;
import com.pc.entities.lookup.Workflow;
import com.pc.entities.lookup.WorkflowRole;

@Repository
public interface WorkflowRoleRepository extends JpaRepository<WorkflowRole, Integer> 
{
	String FIND_UPLOAD_WORKFLOWROLE="SELECT o FROM WorkflowRole o WHERE (o.permission =?2 OR permission =?3) AND o.workflow =?1";
	  
	public WorkflowRole findById(Long parseLong);
	public List<WorkflowRole> findByDescriptionStartingWith(String description);
	public List<WorkflowRole> findByWorkflowOrderByPosition(Workflow wf);
	public Integer countByWorkflow(Workflow workflow);
	@Query(FIND_UPLOAD_WORKFLOWROLE)
	public List<WorkflowRole>  getUploadWorkflowRoles(Workflow wf,PermissionEnum p1,PermissionEnum p2);
}
