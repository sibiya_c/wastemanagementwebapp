package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Workflow;

@Repository
public interface WorkflowRepository extends JpaRepository<Workflow, Integer> 
{
	
	public Workflow findById(Long parseLong);
	public List<Workflow> findByDescriptionStartingWith(String description);
}
