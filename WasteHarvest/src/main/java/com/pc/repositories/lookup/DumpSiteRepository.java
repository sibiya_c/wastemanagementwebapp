package com.pc.repositories.lookup;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.DumpSite;

@Repository
public interface DumpSiteRepository extends JpaRepository<DumpSite, Integer> 
{
	
	public DumpSite findById(Long parseLong);
}
