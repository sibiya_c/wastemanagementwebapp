package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.EmailTemplate;
import com.pc.entities.lookup.Workflow;

@Repository
public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Integer> 
{
	
	public EmailTemplate findById(Long parseLong);
	public List<EmailTemplate> findByDescriptionStartingWith(String description);
	public List<EmailTemplate> findByWorkflow(Workflow wf);
}
