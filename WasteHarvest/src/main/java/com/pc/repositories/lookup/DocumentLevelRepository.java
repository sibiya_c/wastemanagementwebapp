package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.DocumentLevel;

@Repository
public interface DocumentLevelRepository extends JpaRepository<DocumentLevel, Integer> 
{
	
	public DocumentLevel findById(Long parseLong);
	public List<DocumentLevel> findByDescriptionStartingWith(String description);
}
