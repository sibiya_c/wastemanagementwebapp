package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.UserRole;
import com.pc.entities.lookup.WorkFlowRoleUser;
import com.pc.entities.lookup.WorkflowRole;

@Repository
public interface WorkFlowRoleUserRepository extends JpaRepository<WorkFlowRoleUser, Integer> 
{
	public WorkFlowRoleUser findById(Long parseLong);
	public List<WorkFlowRoleUser> findByWorkflowRoleAndUserRole(WorkflowRole workflowRole, UserRole userRole);
	public List<WorkFlowRoleUser> findByWorkflowRole(WorkflowRole wfr);
}
