package com.pc.repositories.lookup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Gender;


@Repository
public interface GenderRepository extends JpaRepository<Gender, Integer> 
{

	public Gender findById(Long parseLong);
	public Gender findByGenderName(String name);
	
	
}
