package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.BBC;

@Repository
public interface BBCRepository extends JpaRepository<BBC, Integer> 
{
	
	public BBC findById(Long parseLong);
	public List<BBC> findByDescriptionStartingWith(String description);
	public List<BBC> findAllByOrderByDescriptionAsc();
	public List<BBC> findAllByOrderByRegionAsc();
}
