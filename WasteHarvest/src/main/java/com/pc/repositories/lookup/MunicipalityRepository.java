package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Municipality;

@Repository
public interface MunicipalityRepository extends JpaRepository<Municipality, Integer> 
{
	
	public Municipality findById(Long parseLong);
	public List<Municipality> findByDescriptionStartingWith(String description);
}
