package com.pc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.entities.enums.EmploymentStatusEnum;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Gender;
import com.pc.entities.lookup.Role;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> 
{
	
	public List<UserRole> findByUser(User user);
	
	public List<UserRole> findByRole(Role role);
	
	public UserRole findById(long parseLong);
	
	String COUNT_BY_ROLE="SELECT COUNT(o) FROM UserRole o WHERE o.role = ?1 AND o.user.bbc = ?2";
	@Query(COUNT_BY_ROLE)
	public long  countByRoleAndBBC(Role role,BBC bbc);
	
	String COUNT_BY_GENDER="SELECT COUNT(o) FROM UserRole o WHERE o.role =?1 AND o.user.gender = ?2 AND o.user.bbc = ?3";
	@Query(COUNT_BY_GENDER)
	public long  countByRoleAndGenderAndBBC(Role role,Gender gender,BBC bbc);
	
	String COUNT_BY_ROLE_AND_EMP_STATUS="SELECT COUNT(o) FROM UserRole o WHERE o.role = ?1 AND o.user.employmentStatus = ?2 AND o.user.bbc = ?3";
	@Query(COUNT_BY_ROLE_AND_EMP_STATUS)
	public long  countByRoleAndEmpStatusAndBBC(Role role,EmploymentStatusEnum status,BBC bbc);
	
	String COUNT_BY_GENDER_AND_EMP_STATU="SELECT COUNT(o) FROM UserRole o WHERE o.role =?1 AND o.user.gender = ?2 AND o.user.employmentStatus = ?3 AND o.user.bbc = ?4";
	@Query(COUNT_BY_GENDER_AND_EMP_STATU)
	public long  countByRoleAndGenderEmpStatusAndBBC(Role role,Gender gender,EmploymentStatusEnum status,BBC bbc);
	

}
