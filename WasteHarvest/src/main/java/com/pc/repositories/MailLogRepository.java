package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.MailLog;

@Repository
public interface MailLogRepository extends JpaRepository<MailLog, Integer> 
{
	
	public MailLog findById(Long parseLong);
	//public List<MailLog> findByDescriptionStartingWith(String description);
}
