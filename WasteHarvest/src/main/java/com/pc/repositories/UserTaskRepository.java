package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.UserTask;

@Repository
public interface UserTaskRepository extends JpaRepository<UserTask, Integer> 
{
	
	public UserTask findById(Long parseLong);
	//public List<UserTask> findByDescriptionStartingWith(String description);
}
