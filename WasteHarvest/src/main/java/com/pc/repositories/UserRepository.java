package com.pc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.pc.beans.LabelTotalBean;
import com.pc.entities.User;
import com.pc.entities.enums.ApprovalStatusEnum;
import com.pc.entities.enums.EmploymentStatusEnum;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Gender;
import com.pc.entities.lookup.Region;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> 
{
	@Query("select c from User c where c.email = ?1")
    public User getUserByEmail(String email);
	
	@Query("select c from User c where c.rsaId = ?1")
    public User getUserByRsaId(String rsaId);
	
	public User findByEmail(String email);
	
	public User findByEmailAndRsaIdNot(String email,String rsaId);
	
	String FIND_CUSTOMER="SELECT o FROM User o WHERE (UPPER(o.name) LIKE CONCAT(?1, '%') OR UPPER(o.surname) LIKE CONCAT(?1, '%') OR UPPER(o.rsaId) LIKE CONCAT(?1, '%')) AND o.customer =?2";
	@Query(FIND_CUSTOMER)
	public List<User> searchUser(String searchText,Boolean customer);
	
	@Query("select c from User c where c.customer =?1")
	public List<User> getAllUserByCustomerFlag(Boolean customer);
	
	public User findById(Long parseLong);
	
	String COUNT_BY_BBC="SELECT COUNT(o) FROM User o WHERE o.bbc = ?1";
	@Query(COUNT_BY_BBC)
	public long  countByBBC(BBC bbc);
	
	String COUNT_BY_GENDER="SELECT COUNT(o) FROM User o WHERE o.bbc =?1 AND o.gender = ?2";
	@Query(COUNT_BY_GENDER)
	public long  countByBBCAndGender(BBC bbc,Gender gender);
	
	String COUNT_BY_BBC_AND_EMP_STATUS="SELECT COUNT(o) FROM User o WHERE o.bbc = ?1 AND o.employmentStatus = ?2";
	@Query(COUNT_BY_BBC_AND_EMP_STATUS)
	public long  countByBBCAndEmpStatus(BBC bbc,EmploymentStatusEnum status);
	
	String COUNT_BY_GENDER_AND_EMP_STATU="SELECT COUNT(o) FROM User o WHERE o.bbc =?1 AND o.gender = ?2 AND o.employmentStatus = ?3";
	@Query(COUNT_BY_GENDER_AND_EMP_STATU)
	public long  countByBBCAndGenderEmpStatus(BBC bbc,Gender gender,EmploymentStatusEnum status);
	
	String COUNT_BY_REGION="SELECT COUNT(o) FROM User o WHERE o.bbc.region = ?1";
	@Query(COUNT_BY_REGION)
	public long  countByRegion(Region region);
	
	@Query("SELECT new com.pc.beans.LabelTotalBean(o.bbc.region.description, COUNT(o)) "
			+ "FROM User o GROUP BY o.bbc.region.description")
	public List<LabelTotalBean> getILabelTotalBeanByRegion();
		
	
	

}
