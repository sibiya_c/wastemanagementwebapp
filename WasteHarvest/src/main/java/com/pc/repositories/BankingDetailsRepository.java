package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.BankingDetails;

@Repository
public interface BankingDetailsRepository extends JpaRepository<BankingDetails, Integer> 
{
	
	public BankingDetails findById(Long parseLong);
	//public List<BankingDetails> findByDescriptionStartingWith(String description);
}
