package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.DocByte;

@Repository
public interface DocByteRepository extends JpaRepository<DocByte, Integer> 
{
	
	public DocByte findById(Long parseLong);
	//public List<DocByte> findByDescriptionStartingWith(String description);
}
