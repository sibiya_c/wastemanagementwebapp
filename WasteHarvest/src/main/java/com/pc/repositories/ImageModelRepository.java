package com.pc.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pc.entities.ImageModel;
import com.pc.entities.User;

@Repository
public interface ImageModelRepository extends JpaRepository<ImageModel, Long>{
	
	@Query("select c from ImageModel c where c.id = ?1")
    public ImageModel getById(Long id);
}
