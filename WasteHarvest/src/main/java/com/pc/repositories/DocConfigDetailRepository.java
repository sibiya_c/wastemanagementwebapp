package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.DocConfigDetail;

@Repository
public interface DocConfigDetailRepository extends JpaRepository<DocConfigDetail, Integer> 
{
	
	public DocConfigDetail findById(Long parseLong);
	//public List<DocConfigDetail> findByDescriptionStartingWith(String description);
}
