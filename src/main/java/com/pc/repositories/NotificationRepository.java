package com.pc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.pc.entities.Notification;
import com.pc.entities.User;
import com.pc.entities.UserRole;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Integer> 
{
	public List<Notification> findByToUserAndViewed(User to,Boolean viewed);
	public Notification findByGuide(String guide);

}
