package com.pc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import com.pc.entities.User;
import com.pc.entities.lookup.Category;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> 
{
	@Query("select c from User c where c.email = ?1")
    public User getUserByEmail(String email);
	
	@Query("select c from User c where c.rsaId = ?1")
    public User getUserByRsaId(String rsaId);
	
	public User findByEmail(String email);
	
	public User findByEmailAndRsaIdNot(String email,String rsaId);
	
	String FIND_CUSTOMER="SELECT o FROM User o WHERE (UPPER(o.name) LIKE CONCAT(?1, '%') OR UPPER(o.surname) LIKE CONCAT(?1, '%') OR UPPER(o.rsaId) LIKE CONCAT(?1, '%')) AND o.customer =?2";
	@Query(FIND_CUSTOMER)
	public List<User> searchUser(String searchText,Boolean customer);
	
	@Query("select c from User c where c.customer =?1")
	public List<User> getAllUserByCustomerFlag(Boolean customer);
	
	public User findById(Long parseLong);
	
	

}
