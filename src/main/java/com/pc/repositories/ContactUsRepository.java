package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.ContactUs;

@Repository
public interface ContactUsRepository extends JpaRepository<ContactUs, Integer> 
{
	
	public ContactUs findById(Long parseLong);
	//public List<ContactUs> findByDescriptionStartingWith(String description);
}
