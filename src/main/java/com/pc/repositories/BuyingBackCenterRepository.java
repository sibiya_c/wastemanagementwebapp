package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.BuyingBackCenter;

@Repository
public interface BuyingBackCenterRepository extends JpaRepository<BuyingBackCenter, Integer> 
{
	
	public BuyingBackCenter findById(Long parseLong);
	//public List<BuyingBackCenter> findByDescriptionStartingWith(String description);
}
