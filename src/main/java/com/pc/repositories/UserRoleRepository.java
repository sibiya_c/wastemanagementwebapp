package com.pc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.entities.lookup.Role;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> 
{
	
	public List<UserRole> findByUser(User user);
	public List<UserRole> findByRole(Role role);
	

}
