package com.pc.repositories.lookup;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.City;
import com.pc.entities.lookup.Province;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> 
{
	
	public City findById(Long parseLong);
	public List<City> findByProvince(Province province);
}
