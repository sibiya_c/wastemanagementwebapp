package com.pc.repositories.lookup;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> 
{
	
	public Item findById(Long parseLong);
	
	public List<Item> findByCategory(Category category);
}
