package com.pc.repositories.lookup;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Services;

@Repository
public interface ServicesRepository extends JpaRepository<Services, Integer> 
{
	
	public Services findById(Long parseLong);
}
