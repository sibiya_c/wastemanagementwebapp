package com.pc.repositories.lookup;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, Integer> 
{

	public Province findById(long id);
	

}
