package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pc.beans.LabelTotalBean;
import com.pc.entities.CustomerProduct;

@Repository
public interface CustomerProductRepository extends JpaRepository<CustomerProduct, Integer> 
{
	
	public CustomerProduct findById(Long parseLong);
	public List<CustomerProduct> findByParentCustomerProduct(CustomerProduct parentCustomerProduct);
	//public List<CustomerProduct> findByDescriptionStartingWith(String description);
	@Query("SELECT new com.pc.beans.LabelTotalBean(o.item.description, SUM(o.weightInKG)) "
			+ "FROM CustomerProduct o GROUP BY o.item.description")
	public List<LabelTotalBean> getItemBean();
	
	@Query("SELECT new com.pc.beans.LabelTotalBean(o.item.category.title, SUM(o.weightInKG)) "
			+ "FROM CustomerProduct o GROUP BY o.item.category.title")
	public List<LabelTotalBean> getProductCategoryBean();
}
