package com.pc.service.lookup;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.lookup.Gender;
import com.pc.repositories.lookup.GenderRepository;

@Service
public class GenderService {
	@Autowired
	GenderRepository repository;
	
	public void saveGender(Gender gender)
	{
		if(gender.getId()==null)
		{
			gender.setCreateDate(new Date());
			
		}
		
		repository.saveAndFlush(gender);
		
	}
	
	public Gender findByGenderName(String name) {
		return repository.findByGenderName(name);
	}
	
	public void deleteGender(Gender gender)
	{
		repository.delete(gender);
	}
	
	/*public void deleteGenderByID(Integer  arg0)
	{
		 repository.delete(arg0);
	}
	*/
	public List<Gender> findAllGender()
	{
		return repository.findAll();
	}
	
	public Page<Gender> findAllGender(Pageable p)
	{
		return repository.findAll(p);
	}
	
	public List<Gender> findAllGender(Sort s)
	{
		return repository.findAll(s);
	}



	public Gender findById(Long parseLong) {
		return repository.findById(parseLong);
	}
	
	/*public Gender findOnelGender(Integer  arg0)
	{
		return repository.findOne(arg0);
	}*/
	

}
