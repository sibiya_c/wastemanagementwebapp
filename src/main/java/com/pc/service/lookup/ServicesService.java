package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.ImageModel;
import com.pc.entities.lookup.Services;
import com.pc.repositories.lookup.ServicesRepository;
import com.pc.service.ImagesService;

@Service
public class ServicesService {
	@Autowired
	ServicesRepository repository;
	
	@Autowired
	ImagesService imagesService;
	
	
	public void saveServices(Services services, ImageModel imageModel)  throws Exception
	{
	    if(services.getId()==null)
		{
			services.setCreateDate(new Date());
			imageModel  = imagesService.save(imageModel);
			services.setImage(imageModel);
			repository.save(services);
		}
	    else
	    {
	    	ImageModel newImg=services.getImage();
			newImg.setName(imageModel.getName());
			newImg.setType(imageModel.getType());
			newImg.setPic(imageModel.getPic());
			services.setImage(newImg);
			repository.save(services);
	    }
	    
		
	}
	
	public void deleteServices(Services services)  throws Exception
	{
		repository.delete(services);
	}
	
	public void deleteServicesByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Services> findAllServices()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Services> findAllServices(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Services> findAllServices(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Services findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	

}
