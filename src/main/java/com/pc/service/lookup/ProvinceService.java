package com.pc.service.lookup;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.lookup.Province;
import com.pc.repositories.lookup.ProvinceRepository;

@Service
public class ProvinceService {
	@Autowired
	ProvinceRepository repository;
	
	public void saveProvince(Province province) throws Exception
	{
	    if(province.getId()==null)
		{
			province.setCreateDate(new Date());
		}
		repository.save(province);
	}
	
	public void deleteProvince(Province province)  throws Exception
	{
		repository.delete(province);
	}
	
	public void deleteProvinceByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Province> findAllProvince()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Province> findAllProvince(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Province> findAllProvince(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}

	public Province findById(long id)  throws Exception
	{
		// TODO Auto-generated method stub
		return repository.findById(id);
	}
	
	
	

}
