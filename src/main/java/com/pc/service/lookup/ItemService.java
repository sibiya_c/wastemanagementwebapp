package com.pc.service.lookup;

import java.util.List;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.ImageModel;
import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Item;
import com.pc.repositories.lookup.ItemRepository;
import com.pc.service.ImagesService;

@Service
public class ItemService {
	@Autowired
	ItemRepository repository;
	
	@Autowired
	ImagesService imagesService;
	
	public void saveItem(Item item)  throws Exception
	{
	    if(item.getId()==null)
		{
			item.setCreateDate(new Date());
		}
		repository.save(item);
	}
	
	public void saveImage(Item item, FileUploadEvent event) throws Exception {
		ImageModel imageModel =  new ImageModel();
		imageModel.setName(event.getFile().getFileName().trim());
		imageModel.setType(event.getFile().getContentType());
		imageModel.setPic(event.getFile().getContents());
		imageModel  = imagesService.save(imageModel);
		item.setImage(imageModel);
		saveItem(item);
		
	}
	
	public void deleteItem(Item item)  throws Exception
	{
		repository.delete(item);
	}
	
	public void deleteItemByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Item> findAllItem()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Item> findAllItem(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Item> findAllItem(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Item findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Item> findByCategory(Category category) throws Exception
	{
		return repository.findByCategory(category);
	}
	
	

}
