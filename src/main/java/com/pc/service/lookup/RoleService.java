package com.pc.service.lookup;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.lookup.Role;
import com.pc.repositories.lookup.RoleRepository;

@Service
public class RoleService {
	@Autowired
	RoleRepository repository;
	
	public void saveRole(Role role)
	{
		if(role.getId()==null)
		{
			role.setCreateDate(new Date());
		}
		repository.save(role);
	}
	
	public void deleteRole(Role role)
	{
		repository.delete(role);
	}
	
	/*public void deleteRoleByID(Integer  arg0)
	{
		 repository.delete(arg0);
	}*/
	
	public List<Role> findAllRole()
	{
		return repository.findAll();
	}
	
	public Page<Role> findAllRole(Pageable p)
	{
		return repository.findAll(p);
	}
	
	public List<Role> findAllRole(Sort s)
	{
		return repository.findAll(s);
	}
	
	public Optional<Role> findOnelRole(Integer  arg0)
	{
		return repository.findById(arg0);
	}
	
	
	
	public Role getOne(Integer id)
	{
		return repository.getOne(id);
	}
	
	public Role findById(Long id)
	{
		return repository.findById(id);
	}
	
	public Role findByCode(String code)
	{
		return repository.findByCode(code);
	}
	
	
	
	
	
	

}
