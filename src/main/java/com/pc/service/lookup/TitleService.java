package com.pc.service.lookup;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.lookup.Title;
import com.pc.repositories.lookup.TitleRepository;

@Service
public class TitleService {
	@Autowired
	TitleRepository repository;
	
	public void saveTitle(Title title)
	{
		if(title.getId()==null)
		{
			title.setCreateDate(new Date());
		}
		repository.save(title);
	}
	
	public void deleteTitle(Title title)
	{
		repository.delete(title);
	}
	
	/*public void deleteTitleByID(Integer  arg0)
	{
		 repository.delete(arg0);
	}*/
	
	public List<Title> findAllTitle()
	{
		return repository.findAll();
	}
	
	public Page<Title> findAllTitle(Pageable p)
	{
		return repository.findAll(p);
	}
	
	public List<Title> findAllTitle(Sort s)
	{
		return repository.findAll(s);
	}

	public Object findById(Long parseLong) {
		return repository.findById(parseLong);
	}
	
	/*public Title findOnelTitle(Integer  arg0)
	{
		return repository.findOne(arg0);
	}*/
	

}
