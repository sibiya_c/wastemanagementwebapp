package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.beans.Mail;
import com.pc.entities.ContactUs;
import com.pc.entities.User;
import com.pc.entities.lookup.AppProperty;
import com.pc.mail.MailSender;
import com.pc.repositories.ContactUsRepository;
import com.pc.service.lookup.AppPropertyService;

@Service
public class ContactUsService {
	@Autowired
	ContactUsRepository repository;
	@Autowired
	MailSender mailSender;
	
	@Autowired
	AppPropertyService appPropertyService;
	
	public void saveContactUs(ContactUs contactUs)  throws Exception
	{
	    if(contactUs.getId()==null)
		{
			contactUs.setCreateDate(new Date());
		}
		repository.save(contactUs);
		sendContactUsEmail(contactUs);
		
	}
	
	public void sendContactUsEmail(ContactUs contactUs) throws Exception {
		AppProperty appProperty=appPropertyService.findByCode("CONTACT_US_EMAIL");
		if(appProperty==null)
		{
			throw new Exception("Contact us email not found");
		}
		String welcome = "<p>Good Day</p>" 
				+ "<p>"+contactUs.getMessage()+" </p>"
	            + "<br/>" 
	        	+ "<p><b>From :</b>"+contactUs.getName()+" "+contactUs.getSurname()+" </p>"
	            + "<p><b>Email :</b>"+contactUs.getEmail()+" </p>"
	            + "<p><b>Cell Number :</b>"+contactUs.getCellNumber()+" </p>"
				+ "<p>Regards</p>" 
				+ "<p>The WM Team</p>"
				+ "<br/>";
		
		Mail mail=new Mail();
		
		mail.setContent(welcome);
		mail.setFrom(contactUs.getEmail());
		String[] to={appProperty.getValue()};
		mail.setTo(to);
		mail.setSubject("Tulsaspark Contact Us Email");
		mail.setCc(to);
		mailSender.sendHtmlEmil(mail);
	}
	
	public void deleteContactUs(ContactUs contactUs)  throws Exception
	{
		repository.delete(contactUs);
	}
	
	public void deleteContactUsByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<ContactUs> findAllContactUs()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<ContactUs> findAllContactUs(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<ContactUs> findAllContactUs(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public ContactUs findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	/*public List<ContactUs> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
