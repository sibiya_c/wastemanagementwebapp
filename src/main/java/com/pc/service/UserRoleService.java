package com.pc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.entities.lookup.Role;
import com.pc.repositories.UserRoleRepository;
import com.pc.service.lookup.RoleService;

@Service
public class UserRoleService {
	@Autowired
	UserRoleRepository repository;
	
	@Autowired
	RoleService roleService;
	
	public void saveUserRole(UserRole userRole) throws Exception
	{
		repository.save(userRole);
	}
	
	public void deleteUserRole(UserRole userRole) throws Exception
	{
		repository.delete(userRole);
	}
	/*
	public void deleteUserRoleByID(Integer  arg0) throws Exception
	{
		 repository.delete(arg0);
	}*/
	
	public List<UserRole> findAllUserRole() throws Exception
	{
		return repository.findAll();
	}
	
	public List<UserRole> findByUser(User user) throws Exception
	{
		return repository.findByUser(user);
	}
	
	public List<UserRole> findByRole(Role role) throws Exception
	{
		return repository.findByRole(role);
	}
	
	public Page<UserRole> findAllUserRole(Pageable p) throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<UserRole> findAllUserRole(Sort s) throws Exception
	{
		return repository.findAll(s);
	}
	
	public void saveAll(ArrayList<UserRole> userRoleList)
	{
		repository.saveAll(userRoleList);
	}
	
	public void deleteAll(List<UserRole>  arg0) throws Exception
	{
	    repository.deleteAll(arg0);
	}
	
	public List<UserRole> findCustomers() throws Exception
	{
		Role role=roleService.findByCode("CUSTOMER");
		return repository.findByRole(role);
	}
	

}
