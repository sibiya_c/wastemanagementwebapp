package com.pc.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pc.entities.ImageModel;
import com.pc.entities.User;
import com.pc.repositories.ImageModelRepository;

@Service
public class ImagesService {
	@Autowired
	ImageModelRepository  imageModelRepository;
	
	public ImageModel save(ImageModel imageModel)throws Exception
	{
		return imageModelRepository.save(imageModel);		
	}
	
	public ImageModel getById(Long id)throws Exception
	{
		return imageModelRepository.getById(id);
	}

}
