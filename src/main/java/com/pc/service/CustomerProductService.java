package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.beans.LabelTotalBean;
import com.pc.entities.CustomerProduct;
import com.pc.repositories.CustomerProductRepository;

@Service
public class CustomerProductService {
	@Autowired
	CustomerProductRepository repository;
	
	public void saveCustomerProduct(CustomerProduct customerProduct)  throws Exception
	{
	    if(customerProduct.getId()==null)
		{
			customerProduct.setCreateDate(new Date());
		}
		repository.save(customerProduct);
	}
	
	public void deleteCustomerProduct(CustomerProduct customerProduct)  throws Exception
	{
		repository.delete(customerProduct);
	}
	
	public void deleteCustomerProductByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<CustomerProduct> findAllCustomerProduct()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<CustomerProduct> findAllCustomerProduct(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<CustomerProduct> findAllCustomerProduct(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public CustomerProduct findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public void deleteAllCustomerProduct(List<CustomerProduct> list)  throws Exception
	{
		repository.deleteAll(list);
	}
	
	public void saveAllCustomerProduct(List<CustomerProduct> list)  throws Exception
	{
		repository.saveAll(list);
	}
	
	public List<CustomerProduct> findByParentCustomerProduct(CustomerProduct parentCustomerProduct) throws Exception
	{
		return repository.findByParentCustomerProduct(parentCustomerProduct);
	}
	
	public List<LabelTotalBean> getItemBean()
	{
		return repository.getItemBean();
	}
	
	
	public List<LabelTotalBean> getProductCategoryBean()
	{
		return repository.getProductCategoryBean();
	}
	
	
	/*public List<CustomerProduct> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
