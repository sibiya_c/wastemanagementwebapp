package com.pc.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import com.pc.entities.BuyingBackCenter;
import com.pc.entities.CustomerProduct;
import com.pc.entities.User;
import com.pc.entities.enums.BbcStatusEnum;
import com.pc.repositories.BuyingBackCenterRepository;

@Service
public class BuyingBackCenterService {
	@Autowired
	BuyingBackCenterRepository repository;
	
	@Autowired
	CustomerProductService customerProductService;
	
	public void saveBuyingBackCenter(BuyingBackCenter buyingBackCenter)  throws Exception
	{
	    if(buyingBackCenter.getId()==null)
		{
			buyingBackCenter.setCreateDate(new Date());
		}
		repository.save(buyingBackCenter);
	}
	
	public void updatePayment(BuyingBackCenter buyingBackCenter)  throws Exception
	{
		BigDecimal balance=BigDecimal.ZERO;
		if (buyingBackCenter.getAmountDue() !=null && buyingBackCenter.getPaidAmount() !=null) {
			balance = new BigDecimal(buyingBackCenter.getAmountDue().longValueExact()
					- buyingBackCenter.getPaidAmount().longValueExact());
		}
		buyingBackCenter.setBalance(balance);
		BbcStatusEnum status=BbcStatusEnum.paid;
		if(balance.longValueExact()>0)
		{
			 status=BbcStatusEnum.outstandingAmount;
		}
		buyingBackCenter.setStatus(status);
		repository.save(buyingBackCenter);
	}
	
	public void saveBuyingBackCenterDeatils(BuyingBackCenter buyingBackCenter,ArrayList<CustomerProduct> customerProductList, User currentUser,User customerUser)  throws Exception
	{
		CustomerProduct customerProductParent=null;
	    if(buyingBackCenter.getId()==null)
		{
	    	if(buyingBackCenter.getBalance()==null)
	    	{
	    		buyingBackCenter.setBalance(BigDecimal.ZERO);
	    	}
	    	buyingBackCenter.setRefNumber("REF"+getUniqueCode());
	    	buyingBackCenter.setStatus(BbcStatusEnum.unpaid);
	    	customerProductParent =new CustomerProduct();
	    	customerProductParent.setCreatorUser(currentUser);
	 	    customerProductParent.setIsParent(true);
	 	    customerProductService.saveCustomerProduct(customerProductParent);
	 	    buyingBackCenter.setParentCustomerProduct(customerProductParent);
	    	buyingBackCenter.setCreatorUser(currentUser);
			buyingBackCenter.setCreateDate(new Date());
			buyingBackCenter.setCustomerUser(customerUser);
			BigDecimal amountDue=BigDecimal.ZERO;
			for(CustomerProduct cp:customerProductList)
			{
				amountDue =new BigDecimal(cp.getTotalCost().longValueExact()+amountDue.longValueExact());
				cp.setCreatorUser(currentUser);
				cp.setParentCustomerProduct(customerProductParent);
				cp.setCustomerUser(customerUser);
				customerProductService.saveCustomerProduct(cp);
			}
			buyingBackCenter.setAmountDue(amountDue);
			repository.save(buyingBackCenter);
		}
	    else
	    {
	    	ArrayList<CustomerProduct>  listToDelete=(ArrayList<CustomerProduct>) customerProductService.findByParentCustomerProduct(buyingBackCenter.getParentCustomerProduct());
	    	
	    	if(listToDelete!=null && listToDelete.size()>0)
	    	{
	    		customerProductService.deleteAllCustomerProduct(listToDelete);
	    	}
	    	BigDecimal amountDue=BigDecimal.ZERO;
	    	for(CustomerProduct cp:customerProductList)
			{
	    		amountDue =new BigDecimal(cp.getTotalCost().longValueExact()+amountDue.longValueExact());
				cp.setCreatorUser(currentUser);
				cp.setParentCustomerProduct(buyingBackCenter.getParentCustomerProduct());
				cp.setCustomerUser(customerUser);
				customerProductService.saveCustomerProduct(cp);
			}
	    	buyingBackCenter.setAmountDue(amountDue);
	    	repository.save(buyingBackCenter);
	    }
	   
		
	}
	
	public String getUniqueCode() {
		  
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 7;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    long millis = System.currentTimeMillis();
	    String generatedString = buffer.toString().toUpperCase()+""+millis;
		return generatedString;
	}
	
	public void deleteBuyingBackCenter(BuyingBackCenter buyingBackCenter)  throws Exception
	{
		repository.delete(buyingBackCenter);
	}
	
	public void deleteBuyingBackCenterByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<BuyingBackCenter> findAllBuyingBackCenter()  throws Exception
	{
		List<BuyingBackCenter> list=repository.findAll();
		for(BuyingBackCenter bbc:list)
		{
			bbc.setCustomerProductList((ArrayList<CustomerProduct>) customerProductService.findByParentCustomerProduct(bbc.getParentCustomerProduct()));
		}
		return list;
	}
	
	public Page<BuyingBackCenter> findAllBuyingBackCenter(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<BuyingBackCenter> findAllBuyingBackCenter(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public BuyingBackCenter findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	/*public List<BuyingBackCenter> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
