package com.pc.mail;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.pc.beans.AttachmentBean;
import com.pc.beans.Mail;
import com.pc.constants.AppConstants;
 
//@Component("javasampleapproachMailSender")
@Component
public class MailSender {
	
	@Autowired
	JavaMailSender javaMailSender;
	
    @Autowired
	private TemplateEngine templateEngine;
	
		
	public void sendMail(Mail mail_info) {

		
		SimpleMailMessage mail = new SimpleMailMessage();
 
		mail.setFrom(mail_info.getFrom());
		mail.setTo(mail_info.getTo());
		mail.setSubject(mail_info.getSubject());
		mail.setText(mail_info.getContent());	
		
		javaMailSender.send(mail);
		
		
	}
	
	 @Autowired
    private JavaMailSender emailSender;
	 
	 
    public void sendSimpleMessageWithAttachment(Mail mail) throws MessagingException 
    {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setSubject(mail.getSubject());
        helper.setText(mail.getContent());
        helper.setTo(mail.getTo());
        helper.setFrom(mail.getFrom());
        
        for (FileSystemResource attachment: mail.getAttachments()) {
            File file = attachment.getFile();
            helper.addAttachment(file.getName(), file);
        }

        /*FileSystemResource file = new FileSystemResource(new File("C:/Users/Patrick/Documents/springbootwebapptemplate/SpringBootWebappTemplate/src/main/webapp/images/b1.jpg"));
        helper.addAttachment("CoolImage.jpg", file);*/
    
        emailSender.send(message);
    } 
    
    
    public void sendHtmlEmil(Mail mail) throws MessagingException 
    {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setSubject(mail.getSubject());
        helper.setText(generateMailHtml(mail.getContent()), true);
        helper.setTo(mail.getTo());
        helper.setFrom(mail.getFrom());
        helper.setBcc(mail.getCc());
        if(mail.getAttachments() !=null && mail.getAttachments().size()>0)
        {
	        for (FileSystemResource attachment: mail.getAttachments()) {
	            File file = attachment.getFile();
	            helper.addAttachment(file.getName(), file);
	        }
        }

       emailSender.send(message);
    } 

    public String generateMailHtml(String body)
    {
        final String templateFileName = "content";//Name of the HTML template file without extension
        
        String output = this.templateEngine.process(templateFileName, new Context(Locale.getDefault(), null));
       
        output=output.replace("#BODY#", body);
        return output;
    }


	public void sendWithBeanAttachement(Mail mail) throws Exception {
		
		  MimeMessage message = emailSender.createMimeMessage();
	        MimeMessageHelper helper = new MimeMessageHelper(message, true);

	        helper.setSubject(mail.getSubject());
	        helper.setText(generateMailHtml(mail.getContent()), true);
	        helper.setTo(mail.getTo());
	        helper.setFrom(mail.getFrom());
	        if(mail.getCc() !=null)
	        {
	        	helper.setBcc(mail.getCc());
	        }
	      
	     
	        if(mail.getAttachmentBeanList() !=null && mail.getAttachmentBeanList().size()>0)
	        {
		        for (AttachmentBean attachment: mail.getAttachmentBeanList()) {
		        	
		        	helper.addAttachment(attachment.getFileName(), attachment.getInputStreamSource(), attachment.getContentType());
		        }
	        }

	       emailSender.send(message);
	}
 

}
