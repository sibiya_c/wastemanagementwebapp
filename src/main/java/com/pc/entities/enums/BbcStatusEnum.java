package com.pc.entities.enums;

public enum BbcStatusEnum {

  paid("Paid"), 
  unpaid("Unpaid"),
  outstandingAmount("Outstanding Amount");

  private String displayName;

  private BbcStatusEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
