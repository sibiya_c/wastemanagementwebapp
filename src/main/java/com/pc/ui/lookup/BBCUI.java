package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.Address;
import com.pc.entities.lookup.BBC;
import com.pc.entities.lookup.City;
import com.pc.entities.lookup.DumpSite;
import com.pc.entities.lookup.Province;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.BBCService;
import com.pc.service.lookup.CityService;

@Component("bBCUI")
@ViewScoped
public class BBCUI extends AbstractUI{

	@Autowired
	BBCService bBCService;
	private ArrayList<BBC> bBCList;
	
	@Autowired
	CityService cityService;
	
	private MapModel simpleModel;
	  
	private Marker marker;


	private BBC bBC;
	
	private Address address;
	
	private Province province;
	
	private String mapCenter;
	private int mapZoom;

	@PostConstruct
	public void init() {
		bBC = new BBC();
		bBCList=(ArrayList<BBC>) findAllBBC();
		address=new Address();
		mapCenter="-30.310268,23.443609";
		mapZoom=6;
		loadBBCs();
	}
	
	
	public void loadBBCs()
	{
		simpleModel = new DefaultMapModel();
        for(BBC  bbc :bBCList)
        {
        	if(province !=null)
        	{
        		if(bbc.getMunicipality().getProvince().equals(province))
        		{
        			 mapZoom=8;
        			 mapCenter=""+province.getLatitude()+","+province.getLongitude()+"";
		        	 LatLng coord = new LatLng(bbc.getLatitude(),bbc.getLongitude());
		        	 simpleModel.addOverlay(new Marker(coord, bbc.getDescription(),bbc));
        		}
        	}
        	else
        	{
        		 mapZoom=6;
        		 mapCenter="-30.310268,23.443609";
        		 LatLng coord = new LatLng(bbc.getLatitude(),bbc.getLongitude());
	        	 simpleModel.addOverlay(new Marker(coord, bbc.getDescription(),bbc));
        	}
        }
	}
	
	
	   public void onMarkerSelect(OverlaySelectEvent event) {
	        marker = (Marker) event.getOverlay();
	        bBC=(BBC) marker.getData();
	    }

	public void saveBBC()
	{
		try {
			bBCService.saveBBC(bBC,address);
			displayInfoMssg("Update Successful...!!");
			bBCList=(ArrayList<BBC>) findAllBBC();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void preparUpdate()
	{
		address=bBC.getResidentialAddress();
		if(address==null)
		{
			address=new Address();
		}
	}

	public void onPointSelect(PointSelectEvent event) {
        LatLng latlng = event.getLatLng();
        bBC.setLatitude(latlng.getLat());
        bBC.setLongitude(latlng.getLng());
        displayInfoMssg("location Selected");
    }
	
	
	public void deleteBBC()
	{
		try {
			bBCService.deleteBBC(bBC);
			displayWarningMssg("Update Successful...!!");
			bBCList=(ArrayList<BBC>) findAllBBC();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<BBC> findAllBBC()
	{
		List<BBC> list=new ArrayList<>();
		try {
			list= bBCService.findAllBBC();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<BBC> findAllBBCPageable()
	{
		Pageable p=null;
		try {
			return bBCService.findAllBBC(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<BBC> findAllBBCSort()
	{
		Sort s=null;
		List<BBC> list=new ArrayList<>();
		try {
			list =bBCService.findAllBBC(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	 public List<City> getSelectItemsCity() {
			
			List<City> l = null;
			try {
				
				if(bBC !=null && bBC.getMunicipality() !=null)
				{
					l = cityService.findByProvince(bBC.getMunicipality().getProvince());
				}
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
	
	public void reset() {
		bBC = new BBC();
		address=new Address();
	}
	

	public BBC getBBC() {
		return bBC;
	}

	public void setBBC(BBC bBC) {
		this.bBC = bBC;
	}
	
	public ArrayList<BBC> getBBCList() {
		return bBCList;
	}

	public void setBBCList(ArrayList<BBC> bBCList) {
		this.bBCList =bBCList;
	}

	public BBC getbBC() {
		return bBC;
	}

	public void setbBC(BBC bBC) {
		this.bBC = bBC;
	}

	public ArrayList<BBC> getbBCList() {
		return bBCList;
	}

	public void setbBCList(ArrayList<BBC> bBCList) {
		this.bBCList = bBCList;
	}

	public CityService getCityService() {
		return cityService;
	}

	public void setCityService(CityService cityService) {
		this.cityService = cityService;
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getMapCenter() {
		return mapCenter;
	}

	public void setMapCenter(String mapCenter) {
		this.mapCenter = mapCenter;
	}

	public int getMapZoom() {
		return mapZoom;
	}

	public void setMapZoom(int mapZoom) {
		this.mapZoom = mapZoom;
	}



}
