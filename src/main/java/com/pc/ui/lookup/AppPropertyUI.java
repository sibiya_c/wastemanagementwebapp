package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.lookup.AppProperty;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.AppPropertyService;

@Component("appPropertyUI")
@ViewScoped
public class AppPropertyUI extends AbstractUI{

	@Autowired
	AppPropertyService appPropertyService;
	private ArrayList<AppProperty> appPropertyList;

	private AppProperty appProperty;

	@PostConstruct
	public void init() {
		appProperty = new AppProperty();
		appPropertyList=(ArrayList<AppProperty>) findAllAppProperty();
	}

	public void saveAppProperty()
	{
		try {
			appPropertyService.saveAppProperty(appProperty);
			displayInfoMssg("Update Successful...!!");
			appPropertyList=(ArrayList<AppProperty>) findAllAppProperty();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteAppProperty()
	{
		try {
			appPropertyService.deleteAppProperty(appProperty);
			displayWarningMssg("Update Successful...!!");
			appPropertyList=(ArrayList<AppProperty>) findAllAppProperty();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<AppProperty> findAllAppProperty()
	{
		List<AppProperty> list=new ArrayList<>();
		try {
			list= appPropertyService.findAllAppProperty();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<AppProperty> findAllAppPropertyPageable()
	{
		Pageable p=null;
		try {
			return appPropertyService.findAllAppProperty(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<AppProperty> findAllAppPropertySort()
	{
		Sort s=null;
		List<AppProperty> list=new ArrayList<>();
		try {
			list =appPropertyService.findAllAppProperty(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		appProperty = new AppProperty();
	}
	

	public AppProperty getAppProperty() {
		return appProperty;
	}

	public void setAppProperty(AppProperty appProperty) {
		this.appProperty = appProperty;
	}
	
	public ArrayList<AppProperty> getAppPropertyList() {
		return appPropertyList;
	}

	public void setAppPropertyList(ArrayList<AppProperty> appPropertyList) {
		this.appPropertyList =appPropertyList;
	}

}
