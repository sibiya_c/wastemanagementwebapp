package com.pc.ui.lookup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.ImageModel;
import com.pc.entities.lookup.Services;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.ServicesService;

@Component("servicesUI")
@ViewScoped
@RequestScoped
public class ServicesUI extends AbstractUI{

	@Autowired
	ServicesService userService;
	private ArrayList<Services> servicesList;
	
	private ImageModel imageModel;

	private Services services;

	@PostConstruct
	public void init() {
		services = new Services();
		servicesList=(ArrayList<Services>) findAllServices();
	}

	public void saveServices()
	{
		try {
			if(imageModel==null)
			{
				throw new Exception("Upload service impage");
			}
			userService.saveServices(services,imageModel);
			displayInfoMssg("Update Successful...!!");
			servicesList=(ArrayList<Services>) findAllServices();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteServices()
	{
		try {
			userService.deleteServices(services);
			displayWarningMssg("Update Successful...!!");
			servicesList=(ArrayList<Services>) findAllServices();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Services> findAllServices()
	{
		List<Services> list=new ArrayList<>();
		try {
			list= userService.findAllServices();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Services> findAllServicesPageable()
	{
		Pageable p=null;
		try {
			return userService.findAllServices(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Services> findAllServicesSort()
	{
		Sort s=null;
		List<Services> list=new ArrayList<>();
		try {
			list =userService.findAllServices(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
   public void handleFileUpload(FileUploadEvent event) {	
		
		try {			
		    imageModel =  new ImageModel();
			imageModel.setName(event.getFile().getFileName().trim());
			imageModel.setType(event.getFile().getContentType());
			imageModel.setPic(event.getFile().getContents());
			displayInfoMssg("Image uploaded...!!");
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}	
	}
   
   public StreamedContent getImageFromDB() {
		try {
			FacesContext context = FacesContext.getCurrentInstance(); 
			if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE || imageModel==null) {
				return new DefaultStreamedContent();
			} else { 
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] image = null;
				try {
					image = imageModel.getPic();
				} catch (Exception e) { 
					displayErrorMssg(e.getMessage());
				} 
				return new DefaultStreamedContent(new ByteArrayInputStream(image),imageModel.getType().trim()); 
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return new DefaultStreamedContent();
		}
	}
	
	public void reset() {
		services = new Services();
		imageModel=null;
	}
	

	public Services getServices() {
		return services;
	}

	public void setServices(Services services) {
		this.services = services;
	}
	
	public ArrayList<Services> getServicesList() {
		return servicesList;
	}

	public void setServicesList(ArrayList<Services> servicesList) {
		this.servicesList =servicesList;
	}

	public ImageModel getImageModel() {
		return imageModel;
	}

	public void setImageModel(ImageModel imageModel) {
		this.imageModel = imageModel;
	}

}
