package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.lookup.Category;
import com.pc.entities.lookup.Item;
import com.pc.framework.AbstractUI;
import com.pc.service.JasperService;
import com.pc.service.lookup.ItemService;

@Component("itemUI")
@ViewScoped
public class ItemUI extends AbstractUI{

	@Autowired
	ItemService itemService;
	private ArrayList<Item> itemList;
	
	private Category category;

	private Item item;

	@PostConstruct
	public void init() {
		item = new Item();
		itemList=(ArrayList<Item>) findAllItem();
	}

	public void saveItem()
	{
		try {
			itemService.saveItem(item);
			displayInfoMssg("Update Successful...!!");
			itemList=(ArrayList<Item>) findAllItem();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void downloadAllItems()
	{
		try {
			itemList=(ArrayList<Item>) findAllItem();
			JasperService.instance().downloadAllItems(itemList);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void handleFileUpload(FileUploadEvent event) {	
		
		try {			
			itemService.saveImage(item, event);
			displayInfoMssg("Image uploaded sucessfully...!!!");	
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}	
	}
	
	public void prepareItemsPerCategory()
	{
		try {
			itemList=(ArrayList<Item>) itemService.findByCategory(category);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteItem()
	{
		try {
			itemService.deleteItem(item);
			displayWarningMssg("Update Successful...!!");
			itemList=(ArrayList<Item>) findAllItem();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Item> findAllItem()
	{
		List<Item> list=new ArrayList<>();
		try {
			list= itemService.findAllItem();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Item> findAllItemPageable()
	{
		Pageable p=null;
		try {
			return itemService.findAllItem(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Item> findAllItemSort()
	{
		Sort s=null;
		List<Item> list=new ArrayList<>();
		try {
			list =itemService.findAllItem(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		item = new Item();
	}
	

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	public ArrayList<Item> getItemList() {
		return itemList;
	}

	public void setItemList(ArrayList<Item> itemList) {
		this.itemList =itemList;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
