package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.lookup.Province;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.ProvinceService;

@Component("provinceUI")
@ViewScoped
public class ProvinceUI extends AbstractUI{

	@Autowired
	ProvinceService userService;
	private ArrayList<Province> provinceList;

	private Province province;

	@PostConstruct
	public void init() {
		province = new Province();
		provinceList=(ArrayList<Province>) findAllProvince();
	}

	public void saveProvince()
	{
		try {
			userService.saveProvince(province);
			displayInfoMssg("Update Successful...!!");
			provinceList=(ArrayList<Province>) findAllProvince();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteProvince()
	{
		try {
			userService.deleteProvince(province);
			displayWarningMssg("Update Successful...!!");
			provinceList=(ArrayList<Province>) findAllProvince();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Province> findAllProvince()
	{
		List<Province> list=new ArrayList<>();
		try {
			list= userService.findAllProvince();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Province> findAllProvincePageable()
	{
		Pageable p=null;
		try {
			return userService.findAllProvince(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Province> findAllProvinceSort()
	{
		Sort s=null;
		List<Province> list=new ArrayList<>();
		try {
			list =userService.findAllProvince(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		province = new Province();
	}
	

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}
	
	public ArrayList<Province> getProvinceList() {
		return provinceList;
	}

	public void setProvinceList(ArrayList<Province> provinceList) {
		this.provinceList =provinceList;
	}

}
