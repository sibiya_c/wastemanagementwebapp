package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.lookup.Title;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.TitleService;

@Component("titleUI")
@ViewScoped
public class TitleUI extends AbstractUI{

	@Autowired
	TitleService userService;
	private ArrayList<Title> titleList;

	private Title title;

	@PostConstruct
	public void init() {
		title = new Title();
		titleList=(ArrayList<Title>) findAllTitle();
	}

	public void saveTitle()
	{
		try {
			userService.saveTitle(title);
			displayInfoMssg("Update Successful...!!");
			titleList=(ArrayList<Title>) findAllTitle();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteTitle()
	{
		try {
			userService.deleteTitle(title);
			displayWarningMssg("Update Successful...!!");
			titleList=(ArrayList<Title>) findAllTitle();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Title> findAllTitle()
	{
		List<Title> list=new ArrayList<>();
		try {
			list= userService.findAllTitle();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Title> findAllTitlePageable()
	{
		Pageable p=null;
		try {
			return userService.findAllTitle(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Title> findAllTitleSort()
	{
		Sort s=null;
		List<Title> list=new ArrayList<>();
		try {
			list =userService.findAllTitle(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		title = new Title();
	}
	

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}
	
	public ArrayList<Title> getTitleList() {
		return titleList;
	}

	public void setTitleList(ArrayList<Title> titleList) {
		this.titleList =titleList;
	}

}
