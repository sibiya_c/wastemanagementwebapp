package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.lookup.Municipality;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.MunicipalityService;

@Component("municipalityUI")
@ViewScoped
public class MunicipalityUI extends AbstractUI{

	@Autowired
	MunicipalityService municipalityService;
	private ArrayList<Municipality> municipalityList;

	private Municipality municipality;

	@PostConstruct
	public void init() {
		municipality = new Municipality();
		municipalityList=(ArrayList<Municipality>) findAllMunicipality();
	}

	public void saveMunicipality()
	{
		try {
			municipalityService.saveMunicipality(municipality);
			displayInfoMssg("Update Successful...!!");
			municipalityList=(ArrayList<Municipality>) findAllMunicipality();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteMunicipality()
	{
		try {
			municipalityService.deleteMunicipality(municipality);
			displayWarningMssg("Update Successful...!!");
			municipalityList=(ArrayList<Municipality>) findAllMunicipality();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Municipality> findAllMunicipality()
	{
		List<Municipality> list=new ArrayList<>();
		try {
			list= municipalityService.findAllMunicipality();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Municipality> findAllMunicipalityPageable()
	{
		Pageable p=null;
		try {
			return municipalityService.findAllMunicipality(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Municipality> findAllMunicipalitySort()
	{
		Sort s=null;
		List<Municipality> list=new ArrayList<>();
		try {
			list =municipalityService.findAllMunicipality(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		municipality = new Municipality();
	}
	

	public Municipality getMunicipality() {
		return municipality;
	}

	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}
	
	public ArrayList<Municipality> getMunicipalityList() {
		return municipalityList;
	}

	public void setMunicipalityList(ArrayList<Municipality> municipalityList) {
		this.municipalityList =municipalityList;
	}

}
