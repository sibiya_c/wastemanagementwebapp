package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.Address;
import com.pc.entities.lookup.City;
import com.pc.entities.lookup.DumpSite;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.CityService;
import com.pc.service.lookup.DumpSiteService;

@Component("dumpSiteUI")
@ViewScoped
public class DumpSiteUI extends AbstractUI{
	
	
	@Autowired
	DumpSiteService userService;
	private ArrayList<DumpSite> dumpSiteList;

	private DumpSite dumpSite;
	
	private MapModel simpleModel;
	  
	private Marker marker;
	
	private Address address;
	
	@Autowired
	CityService cityService;

	@PostConstruct
	public void init() {
		dumpSite = new DumpSite();
		dumpSiteList=(ArrayList<DumpSite>) findAllDumpSite();
		address=new Address();
		loadDumpSites();
	}

	public void saveDumpSite()
	{
		try {
			userService.saveDumpSite(dumpSite,address);
			displayInfoMssg("Update Successful...!!");
			dumpSiteList=(ArrayList<DumpSite>) findAllDumpSite();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void preparUpdate()
	{
		address=dumpSite.getResidentialAddress();
		if(address==null)
		{
			address=new Address();
		}
	}
	public void deleteDumpSite()
	{
		try {
			userService.deleteDumpSite(dumpSite);
			displayWarningMssg("Update Successful...!!");
			dumpSiteList=(ArrayList<DumpSite>) findAllDumpSite();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<DumpSite> findAllDumpSite()
	{
		List<DumpSite> list=new ArrayList<>();
		try {
			list= userService.findAllDumpSite();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<DumpSite> findAllDumpSitePageable()
	{
		Pageable p=null;
		try {
			return userService.findAllDumpSite(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<DumpSite> findAllDumpSiteSort()
	{
		Sort s=null;
		List<DumpSite> list=new ArrayList<>();
		try {
			list =userService.findAllDumpSite(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		dumpSite = new DumpSite();
		address=new Address();
	}
	
	public void onPointSelect(PointSelectEvent event) {
        LatLng latlng = event.getLatLng();
        dumpSite.setLatitude(latlng.getLat());
        dumpSite.setLongitude(latlng.getLng());
        displayInfoMssg("location Selected");
    }
	
	
	public void loadDumpSites()
	{
		simpleModel = new DefaultMapModel();
        for(DumpSite  dumpSite :dumpSiteList)
        {
        	 LatLng coord = new LatLng(dumpSite.getLatitude(),dumpSite.getLongitude());
        	 simpleModel.addOverlay(new Marker(coord, dumpSite.getDescription(),dumpSite));
        }
        
       /* //Shared coordinates
        LatLng coord1 = new LatLng(36.879466, 30.667648);
        LatLng coord2 = new LatLng(36.883707, 30.689216);
        LatLng coord3 = new LatLng(36.879703, 30.706707);
        LatLng coord4 = new LatLng(36.885233, 30.702323);
        //Basic marker
        simpleModel.addOverlay(new Marker(coord1, "Konyaalti"));
        simpleModel.addOverlay(new Marker(coord2, "Ataturk Parki"));
        simpleModel.addOverlay(new Marker(coord3, "Karaalioglu Parki"));
        simpleModel.addOverlay(new Marker(coord4, "Kaleici"));*/
	}
	
	
   public void onMarkerSelect(OverlaySelectEvent event) {
        marker = (Marker) event.getOverlay();
        dumpSite=(DumpSite) marker.getData();
        //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Selected", marker.getTitle()));
    }
   
   public List<City> getSelectItemsCity() {
		
		List<City> l = null;
		try {
			
			if(dumpSite !=null && dumpSite.getMunicipality() !=null)
			{
				l = cityService.findByProvince(dumpSite.getMunicipality().getProvince());
			}
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	

	public DumpSite getDumpSite() {
		return dumpSite;
	}

	public void setDumpSite(DumpSite dumpSite) {
		this.dumpSite = dumpSite;
	}
	
	public ArrayList<DumpSite> getDumpSiteList() {
		return dumpSiteList;
	}

	public void setDumpSiteList(ArrayList<DumpSite> dumpSiteList) {
		this.dumpSiteList =dumpSiteList;
	}

	public DumpSiteService getUserService() {
		return userService;
	}

	public void setUserService(DumpSiteService userService) {
		this.userService = userService;
	}

	public MapModel getSimpleModel() {
		return simpleModel;
	}

	public void setSimpleModel(MapModel simpleModel) {
		this.simpleModel = simpleModel;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
