package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.lookup.City;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.CityService;

@Component("cityUI")
@ViewScoped
public class CityUI extends AbstractUI{

	@Autowired
	CityService userService;
	private ArrayList<City> cityList;

	private City city;

	@PostConstruct
	public void init() {
		city = new City();
		cityList=(ArrayList<City>) findAllCity();
	}

	public void saveCity()
	{
		try {
			userService.saveCity(city);
			displayInfoMssg("Update Successful...!!");
			cityList=(ArrayList<City>) findAllCity();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteCity()
	{
		try {
			userService.deleteCity(city);
			displayWarningMssg("Update Successful...!!");
			cityList=(ArrayList<City>) findAllCity();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<City> findAllCity()
	{
		List<City> list=new ArrayList<>();
		try {
			list= userService.findAllCity();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<City> findAllCityPageable()
	{
		Pageable p=null;
		try {
			return userService.findAllCity(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<City> findAllCitySort()
	{
		Sort s=null;
		List<City> list=new ArrayList<>();
		try {
			list =userService.findAllCity(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		city = new City();
	}
	

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	public ArrayList<City> getCityList() {
		return cityList;
	}

	public void setCityList(ArrayList<City> cityList) {
		this.cityList =cityList;
	}

}
