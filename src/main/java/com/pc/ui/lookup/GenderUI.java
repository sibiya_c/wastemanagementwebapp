package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.lookup.Gender;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.GenderService;

@Component("genderUI")
@ViewScoped
public class GenderUI extends AbstractUI{

	@Autowired
	GenderService service;

	private Gender gender;
	private ArrayList<Gender> genderList=new ArrayList<>();

	@PostConstruct
	public void init() {
		gender = new Gender();
		genderList=(ArrayList<Gender>) findAllGender();
	}

	public void saveGender()
	{
		try {
			
			service.saveGender(gender);
			displaySuccessMssg("Update successful");
			reset();
			genderList=(ArrayList<Gender>) findAllGender();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteGender()
	{
		try {
			service.deleteGender(gender);
			reset();
			genderList=(ArrayList<Gender>) findAllGender();
			displayWarningMssg("Gender deleted successful");
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Gender> findAllGender()
	{
		List<Gender> list=new ArrayList<>();
		try {
			list= service.findAllGender();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Gender> findAllGenderPageable()
	{
		Pageable p=null;
		try {
			return service.findAllGender(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Gender> findAllGenderSort()
	{
		Sort s=null;
		List<Gender> list=new ArrayList<>();
		try {
			list =service.findAllGender(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		gender = new Gender();
	}
	

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public ArrayList<Gender> getGenderList() {
		return genderList;
	}

	public void setGenderList(ArrayList<Gender> genderList) {
		this.genderList = genderList;
	}

}
