package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.pc.framework.AbstractUI;
import com.pc.entities.enums.*;
import com.pc.entities.lookup.*;
import com.pc.entities.*;
import com.pc.service.lookup.*;
import com.pc.service.*;


@Component("commonItemUI")
@ViewScoped
public class CommonItemUI  extends AbstractUI
{
	@Autowired
	GenderService genderService; 
	
	@Autowired
	ProvinceService provinceService;
	
	
	@Autowired
	TitleService titleService;
	
	@Autowired
	CityService cityService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	ItemService itemService;
	
	
	@PostConstruct
	public void init() {
	}
	
	/**
	 * Gets the select items gender.
	 *
	 * @return the select items gender
	 */
	public List<Gender> getSelectItemsGender() {
		
		List<Gender> l = null;
		try {
			
			l = genderService.findAllGender();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	/**
	 * Gets the select items title.
	 *
	 * @return the select items title
	 */
	public List<Title> getSelectItemsTitle() {
		
		List<Title> l = null;
		try {
			
			l = titleService.findAllTitle();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	public List<SelectItem> getYesNoEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (YesNoEnum val : YesNoEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
	
    public List<Province> getSelectItemsProvince() {
		
		List<Province> l = null;
		try {
			
			l = provinceService.findAllProvince();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
    
    public List<Category> getSelectItemsCategory() {
		
		List<Category> l = null;
		try {
			
			l = categoryService.findAllCategory();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
    
   

    public List<City> getSelectItemsCity(Province province) {
		
		List<City> l = null;
		try {
			
			l = cityService.findByProvince(province);
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
    
  
	@Autowired
	BankingDetailsService bankingDetailsService; 
	
	public List<BankingDetails> autoCompleteBankingDetails(String searchText) 
	{
		List<BankingDetails> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=bankingDetailsService.findAllBankingDetails();
			}
			else
			{
				list=bankingDetailsService.findByDescriptionStartingWith(searchText);
			}*/
			list=bankingDetailsService.findAllBankingDetails();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<BankingDetails> getSelectItemsBankingDetails() {
		
		List<BankingDetails> l = null;
		try {
			
			l = bankingDetailsService.findAllBankingDetails();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	


	
	
	@Autowired
	BuyingBackCenterService buyingBackCenterService; 
	
	public List<BuyingBackCenter> autoCompleteBuyingBackCenter(String searchText) 
	{
		List<BuyingBackCenter> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=buyingBackCenterService.findAllBuyingBackCenter();
			}
			else
			{
				list=buyingBackCenterService.findByDescriptionStartingWith(searchText);
			}*/
			list=buyingBackCenterService.findAllBuyingBackCenter();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<BuyingBackCenter> getSelectItemsBuyingBackCenter() {
		
		List<BuyingBackCenter> l = null;
		try {
			
			l = buyingBackCenterService.findAllBuyingBackCenter();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	 
	@Autowired
	UserService userService; 
	
	public List<User> autoCompleteUser(String searchText) 
	{
		List<User> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=userService.getAllUserByCustomerFlag(true);
			}
			else
			{
				list=userService.searchUser(searchText,true);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Item> getSelectItemsItem() {
			
			List<Item> l = null;
			try {
				
				l = itemService.findAllItem();
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
			}
			return l;
		}
		
	
	
	@Autowired
	CustomerProductService customerProductService; 
	
	public List<CustomerProduct> autoCompleteCustomerProduct(String searchText) 
	{
		List<CustomerProduct> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=customerProductService.findAllCustomerProduct();
			}
			else
			{
				list=customerProductService.findByDescriptionStartingWith(searchText);
			}*/
			list=customerProductService.findAllCustomerProduct();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<CustomerProduct> getSelectItemsCustomerProduct() {
		
		List<CustomerProduct> l = null;
		try {
			
			l = customerProductService.findAllCustomerProduct();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	MunicipalityService municipalityService; 
	
	public List<Municipality> autoCompleteMunicipality(String searchText) 
	{
		List<Municipality> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=municipalityService.findAllMunicipality();
			}
			else
			{
				list=municipalityService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Municipality> getSelectItemsMunicipality() {
		
		List<Municipality> l = null;
		try {
			
			l = municipalityService.findAllMunicipality();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	AppPropertyService appPropertyService; 
	
	public List<AppProperty> autoCompleteAppProperty(String searchText) 
	{
		List<AppProperty> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=appPropertyService.findAllAppProperty();
			}
			else
			{
				list=appPropertyService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<AppProperty> getSelectItemsAppProperty() {
		
		List<AppProperty> l = null;
		try {
			
			l = appPropertyService.findAllAppProperty();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	ContactUsService contactUsService; 
	
	public List<ContactUs> autoCompleteContactUs(String searchText) 
	{
		List<ContactUs> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=contactUsService.findAllContactUs();
			}
			else
			{
				list=contactUsService.findByDescriptionStartingWith(searchText);
			}*/
			list=contactUsService.findAllContactUs();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<ContactUs> getSelectItemsContactUs() {
		
		List<ContactUs> l = null;
		try {
			
			l = contactUsService.findAllContactUs();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	BBCService bBCService; 
	
	public List<BBC> autoCompleteBBC(String searchText) 
	{
		List<BBC> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=bBCService.findAllBBC();
			}
			else
			{
				list=bBCService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<BBC> getSelectItemsBBC() {
		
		List<BBC> l = null;
		try {
			
			l = bBCService.findAllBBC();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	//===============DO NOT REMOVE THIS=================//












    
 
	
	

}
