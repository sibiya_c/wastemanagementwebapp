package com.pc.ui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.pc.entities.BankingDetails;
import com.pc.entities.ImageModel;
import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.framework.AbstractUI;
import com.pc.service.BankingDetailsService;
import com.pc.service.ImagesService;
import com.pc.service.UserRoleService;
import com.pc.service.UserService;

@Component
@ManagedBean(name="profileUI")
@ViewScoped
@RequestScoped
public class ProfileUI extends AbstractUI{
	
	private List<UserRole>userRoles;
	

	private String newPassword;
	private String password;

	@Autowired
	UserService userService;	
	@Autowired
	ImagesService imagesService;	
	@Autowired
	UserRoleService roleService;
	@Autowired
	BankingDetailsService bankingDetailsService;
	
	BankingDetails bankingDetails;
	
	@PostConstruct
	public void init() {
		  
		  try
		  {
			prepareCurrentUser();
			prepUserRole();
			bankingDetails=currentUser.getBankingDetails();
			if(bankingDetails==null)
			{

				bankingDetails=new BankingDetails();
			}
		  }
		  catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
	}
	
	
	public void saveBankingDetails()
	{
		try {
			bankingDetailsService.saveBankingDetails(bankingDetails,currentUser);
			displayInfoMssg("Banking Details updated successful");	
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
    
	private void prepUserRole() {
		try {
			userRoles = roleService.findByUser(currentUser);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
	}

	public void handleFileUpload(FileUploadEvent event) {	
		
		try {			
			userService.saveProfileImage(currentUser, event);
			prepareCurrentUser();
			displayInfoMssg("Profile image uploaded successful");	
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}	
	}
	
	public void updateUser()
	{
		  try {
			  if(userService.findByEmailAndRsaIdNot(currentUser.getEmail(),currentUser.getRsaId()) !=null){				  
				  throw new Exception("Email address aready exist, please provide a unique email");
		  }
		  else{
			  userService.saveUser(currentUser);
			  displayInfoMssg("Profile updated sucessfully");
		  }
	} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}  
  
	public void updateUserPassword(){
		try {
			System.out.println("New password " + newPassword + " old password " + password);
			this.currentUser = userService.changePassword(this.currentUser.getEmail(), this.password, this.newPassword);
			displayInfoMssg("Password updated sucessfully");
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
		} 
	}

	public User getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}
    
	public StreamedContent getImageFromDB() throws Exception {
		prepareCurrentUser();
		FacesContext context = FacesContext.getCurrentInstance(); 
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else { 
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] image = null;
			try {
				image = currentUser.getImage().getPic();
			} catch (Exception e) { 
				displayErrorMssg(e.getMessage());
			} 
			return new DefaultStreamedContent(new ByteArrayInputStream(image),currentUser.getImage().getType().trim()); 
		}
	}
	
	public StreamedContent getImageFromDB2() throws Exception {
		prepareCurrentUser();
		FacesContext context = FacesContext.getCurrentInstance(); 
		ImageModel imageModel=null;
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else { 
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			String imageId = context.getExternalContext().getRequestParameterMap().get("imageId");
			byte[] image = null;
			try {
				
			    imageModel=imagesService.getById(Long.parseLong(imageId)-1);
				image = imageModel.getPic();
			} catch (Exception e) { 
				displayErrorMssg(e.getMessage());
			} 
			return new DefaultStreamedContent(new ByteArrayInputStream(image),imageModel.getType().trim()); 
		}
	}


	public BankingDetails getBankingDetails() {
		return bankingDetails;
	}


	public void setBankingDetails(BankingDetails bankingDetails) {
		this.bankingDetails = bankingDetails;
	}    
}
