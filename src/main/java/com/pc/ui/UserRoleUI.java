package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.entities.lookup.Role;
import com.pc.framework.AbstractUI;
import com.pc.service.UserRoleService;
import com.pc.service.UserService;
import com.pc.service.lookup.RoleService;

@Component
@ManagedBean(name="userRoleUI")
@ViewScoped
public class UserRoleUI extends AbstractUI{

	private UserRole userRole;
	private ArrayList<User> userList;
	private User selectedUser;
	private ArrayList<UserRole> selectedUserRoles;
	private ArrayList<UserRole> currentUserRoles;
	private ArrayList<Role> roleList;
	private ArrayList<Role> roleListTarget=new  ArrayList<Role>();
	private ArrayList<Role> roleListSource=new  ArrayList<Role>();
	private DualListModel<Role> rolesDualList;
	ArrayList<Role> currentRoles=new ArrayList<Role>();
	private Boolean showRoleDetails;
	private User user=new User();
	
	@Autowired
	UserRoleService userRoleService;
	@Autowired
	RoleService roleService;
	@Autowired
	UserService userService;
	
	private Boolean showBbc;
	
	

	@PostConstruct
	public void init() {
		
		try {
			userRole = new UserRole();
			userList=(ArrayList<User>) userService.findAllUser();
			roleList=(ArrayList<Role>) roleService.findAllRole();
			rolesDualList = new DualListModel<>(roleList,roleListTarget);
			selectedUserRoles=new ArrayList<>();
			currentUserRoles=new ArrayList<>();
			showRoleDetails=false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}
	
	public void deleteUser()
	{
		try {
			ArrayList<UserRole> userRoles=alluserRoles(user);
			userRoleService.deleteAll(userRoles);
			userService.deleteUser(user);
			userList=(ArrayList<User>) userService.findAllUser();
			displayInfoMssg("User deleted Successful..!!");
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void loadUsers()
	{
		try {
			userList=(ArrayList<User>) userService.findAllUser();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public ArrayList<UserRole> alluserRoles(User user)
	{
		try {
			return (ArrayList<UserRole>) userRoleService.findByUser(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
			return new ArrayList<UserRole>();
		}
	}

	public void saveUserRole()
	{
		try {
			userRoleService.saveUserRole(userRole);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void preparShowBBC()
	{
		for(Role ur:rolesDualList.getTarget())
		{
			
			if(ur.getCode().equalsIgnoreCase("BBC_OP"))
			{
				showBbc=true;
			}
		}
	}
	
	public void saveUserRoles()
	{
		try 
		{
			if(rolesDualList.getTarget().size()<=0)
			{
				throw new Exception("Please select atleast one role before submiting "+rolesDualList.getTarget().size());
			}
			//Deleting current user roles
			boolean isCustomer=false;
			userRoleService.deleteAll(currentUserRoles);
			for(Role ur:rolesDualList.getTarget())
			{
				UserRole userRole=new UserRole();
				userRole.setRole(ur);
				userRole.setUser(selectedUser);
				selectedUserRoles.add(userRole);
				if(ur.getCode().equalsIgnoreCase("EMPLOYEE"))
				{
					isCustomer=true;
				}
				
				
			}
			if(!showBbc)
			{
				selectedUser.setBbc(null);
			}
			selectedUser.setCustomer(isCustomer);
			userService.saveUser(selectedUser);
			userRoleService.saveAll(selectedUserRoles);
			
			clearSelectUser();
			showRoleDetails=false;
			showBbc=false;
		
			
		}catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteUserRole()
	{
		try {
			userRoleService.deleteUserRole(userRole);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<UserRole> findAllUserRole()
	{
		List<UserRole> list=new ArrayList<>();
		try {
			list= userRoleService.findAllUserRole();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<UserRole> findAllUserRolePageable()
	{
		Pageable p=null;
		try {
			return userRoleService.findAllUserRole(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<UserRole> findAllUserRoleSort()
	{
		Sort s=null;
		List<UserRole> list=new ArrayList<>();
		try {
			list =userRoleService.findAllUserRole(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void preperPopup()
	{
		try {
			currentUserRoles.clear();
			currentUserRoles=(ArrayList<UserRole>) userRoleService.findByUser(selectedUser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}
	
	public void loadUserRoles()
	{
		try {
			
			showRoleDetails=true;
			//displayInfoMssg("Loading User data");
			currentUserRoles.clear();
			currentUserRoles=(ArrayList<UserRole>) userRoleService.findByUser(selectedUser);
			currentRoles.clear();
			for(UserRole ur:currentUserRoles){
				currentRoles.add(ur.getRole());
			}
			
			//Clearing roleListSource
			roleListSource.clear();
			for(Role role:roleList)
			{
				boolean hasThisRole=false;
				for(Role r:currentRoles)
				{
					if(r.getId().longValue()==role.getId().longValue())
					{
						hasThisRole=true;
						break;
					}
				}
				if(!hasThisRole)
				{
					roleListSource.add(role);
				}
			}

			rolesDualList = new DualListModel<>(roleListSource,currentRoles);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	 
	
	public void clearSelectUser()
	{
		selectedUser=null;
		roleList=(ArrayList<Role>) roleService.findAllRole();
		rolesDualList = new DualListModel<>(roleList, roleListTarget);
		selectedUserRoles=new ArrayList<>();
		showRoleDetails=false;
		
	}
	
	public void test()
	{
		Role role=roleService.getOne(2);
		String mssg=""+role.getDescription();
		displayInfoMssg(mssg);
	}
	

	
	
	
	public void reset() {
		userRole = new UserRole();
	}
	

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public ArrayList<User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public ArrayList<UserRole> getSelectedUserRoles() {
		return selectedUserRoles;
	}

	public void setSelectedUserRoles(ArrayList<UserRole> selectedUserRoles) {
		this.selectedUserRoles = selectedUserRoles;
	}

	public ArrayList<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(ArrayList<Role> roleList) {
		this.roleList = roleList;
	}

	public DualListModel<Role> getRolesDualList() {
		return rolesDualList;
	}

	public void setRolesDualList(DualListModel<Role> rolesDualList) {
		this.rolesDualList = rolesDualList;
	}

	public ArrayList<UserRole> getCurrentUserRoles() {
		return currentUserRoles;
	}

	public void setCurrentUserRoles(ArrayList<UserRole> currentUserRoles) {
		this.currentUserRoles = currentUserRoles;
	}

	public ArrayList<Role> getRoleListTarget() {
		return roleListTarget;
	}

	public void setRoleListTarget(ArrayList<Role> roleListTarget) {
		this.roleListTarget = roleListTarget;
	}

	public ArrayList<Role> getCurrentRoles() {
		return currentRoles;
	}

	public void setCurrentRoles(ArrayList<Role> currentRoles) {
		this.currentRoles = currentRoles;
	}

	public Boolean getShowRoleDetails() {
		return showRoleDetails;
	}

	public void setShowRoleDetails(Boolean showRoleDetails) {
		this.showRoleDetails = showRoleDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getShowBbc() {
		return showBbc;
	}

	public void setShowBbc(Boolean showBbc) {
		this.showBbc = showBbc;
	}

}
