package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.ContactUs;
import com.pc.framework.AbstractUI;
import com.pc.service.ContactUsService;

@Component("contactUsUI")
@ViewScoped
public class ContactUsUI extends AbstractUI{

	@Autowired
	ContactUsService contactUsService;
	private ArrayList<ContactUs> contactUsList;

	private ContactUs contactUs;

	@PostConstruct
	public void init() {
		contactUs = new ContactUs();
		//contactUsList=(ArrayList<ContactUs>) findAllContactUs();
	}

	public void saveContactUs()
	{
		try {
			contactUsService.saveContactUs(contactUs);
			displayInfoMssg("Update Successful...!!");
			//contactUsList=(ArrayList<ContactUs>) findAllContactUs();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteContactUs()
	{
		try {
			contactUsService.deleteContactUs(contactUs);
			displayWarningMssg("Update Successful...!!");
			contactUsList=(ArrayList<ContactUs>) findAllContactUs();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<ContactUs> findAllContactUs()
	{
		List<ContactUs> list=new ArrayList<>();
		try {
			list= contactUsService.findAllContactUs();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<ContactUs> findAllContactUsPageable()
	{
		Pageable p=null;
		try {
			return contactUsService.findAllContactUs(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ContactUs> findAllContactUsSort()
	{
		Sort s=null;
		List<ContactUs> list=new ArrayList<>();
		try {
			list =contactUsService.findAllContactUs(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		contactUs = new ContactUs();
	}
	

	public ContactUs getContactUs() {
		return contactUs;
	}

	public void setContactUs(ContactUs contactUs) {
		this.contactUs = contactUs;
	}
	
	public ArrayList<ContactUs> getContactUsList() {
		return contactUsList;
	}

	public void setContactUsList(ArrayList<ContactUs> contactUsList) {
		this.contactUsList =contactUsList;
	}

}
