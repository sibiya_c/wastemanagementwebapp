package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import com.pc.entities.BankingDetails;
import com.pc.framework.AbstractUI;
import com.pc.service.BankingDetailsService;

@Component("bankingDetailsUI")
@ViewScoped
public class BankingDetailsUI extends AbstractUI{

	@Autowired
	BankingDetailsService bankingDetailsService;
	private ArrayList<BankingDetails> bankingDetailsList;

	private BankingDetails bankingDetails;

	@PostConstruct
	public void init() {
		bankingDetails = new BankingDetails();
		bankingDetailsList=(ArrayList<BankingDetails>) findAllBankingDetails();
	}

	public void saveBankingDetails()
	{
		try {
			bankingDetailsService.saveBankingDetails(bankingDetails);
			displayInfoMssg("Update Successful...!!");
			bankingDetailsList=(ArrayList<BankingDetails>) findAllBankingDetails();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteBankingDetails()
	{
		try {
			bankingDetailsService.deleteBankingDetails(bankingDetails);
			displayWarningMssg("Update Successful...!!");
			bankingDetailsList=(ArrayList<BankingDetails>) findAllBankingDetails();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<BankingDetails> findAllBankingDetails()
	{
		List<BankingDetails> list=new ArrayList<>();
		try {
			list= bankingDetailsService.findAllBankingDetails();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<BankingDetails> findAllBankingDetailsPageable()
	{
		Pageable p=null;
		try {
			return bankingDetailsService.findAllBankingDetails(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<BankingDetails> findAllBankingDetailsSort()
	{
		Sort s=null;
		List<BankingDetails> list=new ArrayList<>();
		try {
			list =bankingDetailsService.findAllBankingDetails(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		bankingDetails = new BankingDetails();
	}
	

	public BankingDetails getBankingDetails() {
		return bankingDetails;
	}

	public void setBankingDetails(BankingDetails bankingDetails) {
		this.bankingDetails = bankingDetails;
	}
	
	public ArrayList<BankingDetails> getBankingDetailsList() {
		return bankingDetailsList;
	}

	public void setBankingDetailsList(ArrayList<BankingDetails> bankingDetailsList) {
		this.bankingDetailsList =bankingDetailsList;
	}

}
