package com.pc.ui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.pc.entities.BankingDetails;
import com.pc.entities.ImageModel;
import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.framework.AbstractUI;
import com.pc.service.BankingDetailsService;
import com.pc.service.ImagesService;
import com.pc.service.UserRoleService;
import com.pc.service.UserService;

@Component
@ManagedBean(name="streamContentUI")
@ViewScoped
@RequestScoped
public class StreamContentUI extends AbstractUI{
	@Autowired
	ImagesService imagesService;
	
	@PostConstruct
	public void init() {
		  
		  try
		  {
			
		  }
		  catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
	}
	
	 public StreamedContent getImageFromDB()  {
	        try {
				FacesContext context = FacesContext.getCurrentInstance();
				if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
				    // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
				    return new DefaultStreamedContent();
				} else {
				    // So, browser is requesting the media. Return a real StreamedContent with the media bytes.
				    String id = context.getExternalContext().getRequestParameterMap().get("id");
				    if(!id.isEmpty())
				    {
					    ImageModel media = imagesService.getById(Long.valueOf(id));
					    return new DefaultStreamedContent(new ByteArrayInputStream(media.getPic()));
				    }
				    else
				    {
				    	return new DefaultStreamedContent();
				    }
				}
			
			} catch (Exception e) {
				displayErrorMssg(e.getMessage());
				e.printStackTrace();
				return new DefaultStreamedContent();
			}
	    }

	
}
