package com.pc.common;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.pc.beans.Mail;
import com.pc.constants.AppConstants;
import com.pc.mail.MailSender;
@Configuration
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"com.pc"})
@EntityScan("com.pc.entities")
@EnableJpaRepositories("com.pc.repositories")
public class SpringPrimeFacesApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication.run(SpringPrimeFacesApplication.class, args);
  }

  
  @Autowired
  MailSender mailSender;
  
  
	@Override
	public void run(String... args) throws Exception {
		/*Mail mail=new Mail();
		
		mail.setContent("Email Content");
		mail.setFrom("christoph@gmail.com");
		String[] to={"christoph@dajotechnologies.com"};;
		mail.setTo(to);
		mail.setSubject("Test Email");
		mail.setCc(to);
		mailSender.sendHtmlEmil(mail);*/
		
	}
	  
 
}
